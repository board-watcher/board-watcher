
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import regularizers
from tensorflow.keras.optimizers import SGD
from tensorflow.keras.optimizers import Adam
# Helper libraries
import numpy as np
import matplotlib.pyplot as plt
import cv2
import glob
from tensorflow.keras.preprocessing.image import ImageDataGenerator



datagen = ImageDataGenerator(
        rotation_range=40,
        rescale=1./255,
        zoom_range=0.25,
        validation_split=0.2
)


#test_datagen = ImageDataGenerator(rescale=1./255)

'''train_generator  = datagen.flow_from_directory(
    "C:/Users/excel/Desktop/NoweZdjecia/BierkiSpecjalnePoWyrzuceniu",
    target_size=(125,125),
    batch_size=32,
    class_mode='binary',
    subset='training',
    color_mode='rgb'

)

validation_generator = datagen.flow_from_directory(
    "C:/Users/excel/Desktop/NoweZdjecia/BierkiSpecjalnePoWyrzuceniu",
    target_size = (125,125),
    batch_size = 32,
    subset='validation',
    class_mode = 'binary',
    color_mode = 'rgb'
 )'''



train_ds = tf.keras.preprocessing.image_dataset_from_directory(
    "C:/Users/excel/Desktop/NoweZdjecia/BierkiSpecjalnePoWyrzuceniuBierkiFilm",
    validation_split=0.2,
    subset="training",
    seed=123,
    image_size=(125,125),
    batch_size=32,
    color_mode = 'rgb'
)


val_ds = tf.keras.preprocessing.image_dataset_from_directory(
    "C:/Users/excel/Desktop/NoweZdjecia/BierkiSpecjalnePoWyrzuceniuBierkiFilm",
    validation_split=0.2,
    subset="validation",
    seed=123,
    image_size=(125, 125),
    batch_size=32,
    color_mode = 'rgb'
)

test_ds = tf.keras.preprocessing.image_dataset_from_directory(
    "C:/Users/excel/Desktop/NoweZdjecia/halo",
    image_size=(125, 125),
    batch_size=32
) 

# performing data argumentation by training image generator
dataAugmentaion = ImageDataGenerator(rotation_range = 30, zoom_range = 0.20, rescale=1./255,)

w = dataAugmentaion.flow_from_directory(
    "C:/Users/excel/Desktop/NoweZdjecia/BierkiTrening/Bierki 3",
    target_size = (125,125),
    batch_size = 49,
    class_mode = 'binary',
    color_mode = 'rgb'
)

# training the model



base_model = tf.keras.applications.Xception(include_top=False, weights='imagenet', input_shape=(125, 125, 3))

x = base_model.output
x = tf.keras.layers.GlobalAveragePooling2D()(x)

x = tf.keras.layers.Dense(512, activation='relu')(x)
x = tf.keras.layers.Dense(512, activation='relu')(x)
predictions = tf.keras.layers.Dense(13, activation='softmax')(x) # New softmax layer
model = tf.keras.Model(inputs=base_model.input, outputs=predictions)

# we chose to train the top 2 inception blocks
# we will freeze the first 249 layers and unfreeze the rest
for layer in model.layers[:249]:
   tf.keras.layers.trainable = False
for layer in model.layers[249:]:
   tf.keras.layers.trainable = True


adam = Adam(lr=0.0001)
model.compile(adam,
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])

    



model.fit(train_ds, validation_data=val_ds, batch_size=32, epochs=3)







results = model.evaluate(test_ds, batch_size=32)
print("test loss, test acc:", results)
save_locally = tf.saved_model.SaveOptions(experimental_io_device='/job:localhost')
model.save('./model', options=save_locally)
