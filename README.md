Do uruchomienia wymagany jest zainstalowany Python w wersji 3.7

Aby zainstalować potrzebne pakiety wykonaj 

 ``` 
    pip install -r req.txt
 ```

Następnie w konsoli z poziomu głównego katalogu projektu należy wpisać
```
   python bin/boardwatcher.py
```
W przypadku platformy Windows w katalogu dist znajduje się skompilowana wersja aplikacji za pomocą biblioteki PyInstaller,
jednakże działa ona niestabilnie stąd
polecamy jednak używanie aplikacji z poziomu konsoli


Instrukcja obsługi użytkownika znajduje się w katalogu manual
Dokumetacja znajduje się w katalogu doc

Informacje o pozostałych modułach znajdują się na stronie projektu

https://aleks-2.mat.umk.pl/pz2020/zesp02/