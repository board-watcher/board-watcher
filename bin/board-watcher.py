#!/usr/bin/env python
# -*- coding: utf-8 -*-


import sys
import os.path
sys.path.append(str(os.path.abspath(os.path.dirname(__file__)) + '/../'))

from boardwatcher import driver

d = driver.Driver()
d.start()
