import json
import random
import string
from collections import defaultdict

import cv2
import numpy as np
import requests
import tensorflow as tf
from matplotlib import pyplot as plt
from tensorflow import keras
from tkinter import Tk


def segment_by_angle_kmeans(lines, k=2, **kwargs):
    """ Segmentacja linii na poziome/pionowe wykorzystując algorytm kmeans. """
 
    default_criteria_type = cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER
    criteria = kwargs.get('criteria', (default_criteria_type, 10, 1.0))
    flags = kwargs.get('flags', cv2.KMEANS_RANDOM_CENTERS)
    attempts = kwargs.get('attempts', 10)

    angles = np.array([line[0][1] for line in lines])

    pts = np.array([[np.cos(2*angle), np.sin(2*angle)]
                    for angle in angles], dtype=np.float32)

    #num_rows = np.shape(pts)[0]
    #if num_rows > k: /ten if prawda to zrób kmeans, w przeciwnym wypadku wywal jakiś wyjątek TODO
    labels, centers = cv2.kmeans(pts, k, None, criteria, attempts, flags)[1:]
    labels = labels.reshape(-1)
 
    segmented = defaultdict(list)
    for i, line in zip(range(len(lines)), lines):
        segmented[labels[i]].append(line)
    segmented = list(segmented.values())
    return segmented
 
def intersection(line1, line2):
    """ Szukanie punktów przecięć na dwóch liniach """
    rho1, theta1 = line1[0]
    rho2, theta2 = line2[0]
    C = np.array([
        [np.cos(theta1), np.sin(theta1)],
        [np.cos(theta2), np.sin(theta2)]
    ])
    r = np.array([[rho1], [rho2]])
    wyznacznik = (C[0][0] * C[1][1]) - (C[0][1] * C[1][0])
 
    if wyznacznik != 0:
        x1, y1 = np.linalg.solve(C, r)
        x1, y1 = int(np.round(x1)), int(np.round(y1))
        return [x1, y1]
 
def segmentedIntersections(lines):
    """ Dodanie punktow przeciec do listy """
    intersections = []
    for i, group in enumerate(lines[:-1]):
        for next_group in lines[i+1:]:
            for line1 in group:
                for line2 in next_group:
                    if(intersection(line1, line2) != None):
                        intersections.append(intersection(line1, line2)) 
 
    return intersections

def auto_canny(image):
    """ Algorytm Canny-Edge """
    gray = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
    edged = cv2.Canny(gray,100,200,apertureSize = 3)
    return edged
 
def filterIntersections(intersections):
    """ Filtrowanie punktow przecięć, usunięcie punktów przecięć, które są zbyt blisko siebie """
    aux = []
    res = []
 
    for i, point in enumerate(intersections):
        del_count = 0
        aux.append(point)
        for j, iterated_point in enumerate(intersections[i+1:]):
            if(abs(point[0] - iterated_point[0]) < 25 and abs(point[1] - iterated_point[1]) < 25):
                aux.append(iterated_point)
                #intersections.remove(iterated_point)
                del intersections[i + 1 + j - del_count]
                del_count = del_count + 1
 
        x, y = np.mean(aux, axis=0)
        res.append((int(x), int(y)))
        aux = []
 
    return np.array(res)

def pick_points(points):
    """ Zaczynajac od srodka obrazu, tworzenie 4 ćwiartek szachownicy, w zależności od odległosci dodawane są kolejno punkty w celu otrzymania kompletu 81 """
    c = points - [550, 550]
    center_ind = np.argwhere(np.sqrt(np.sum(c*c, axis=1))
                             == np.min(np.sqrt(np.sum(c*c, axis=1))))
    center = points[center_ind][0][0]
    top_x = []
    top_y = []
    bottom_x = []
    bottom_y = []
 
    for point in points:
        if np.all(point == center):
            continue
        if point[0] > center[0] and (point[1] < center[1] + 15 and point[1] > center[1] - 15):
            top_x.append(point)
        if point[0] < center[0] and (point[1] < center[1] + 15 and point[1] > center[1] - 15):
            bottom_x.append(point)
        if point[1] > center[1] and (point[0] < center[0] + 15 and point[0] > center[0] - 15):
            top_y.append(point)
        if point[1] < center[1] and (point[0] < center[0] + 15 and point[0] > center[0] - 15):
            bottom_y.append(point)
 
    top_x = sorted(top_x, key=lambda k: k[0])[:4]
    bottom_x = sorted(bottom_x, key=lambda k: k[0], reverse=True)[:4]
    top_y = sorted(top_y, key=lambda k: k[1])[:4]
    bottom_y = sorted(bottom_y, key=lambda k: k[1], reverse=True)[:4]
 
    picked_points = []
    for x, _ in top_x + bottom_x + [center]:
        for _, y in top_y + bottom_y + [center]:
            picked_points.append([x, y])
 
    return picked_points
 
def splitChessboardImageIntoFields(intersections, img):
    """ Wyciecie pojedynczych pól """
    fields = []
    matrix = np.reshape(intersections, (9, 9, 2))
    for i in range(0, 8):
        for j in range(0, 8):
            if matrix[i][j][0]<550 and matrix[i][j][1]<550:
                source_coords = np.float32(
                    [matrix[i][j]-10, matrix[i][j + 1], matrix[i + 1][j], matrix[i + 1][j + 1]])
            if matrix[i][j][0]<550 and matrix[i][j][1]>550:
    
                source_coords = np.float32(
                    [(matrix[i][j][0]-10,matrix[i][j][1]+10), matrix[i][j + 1], matrix[i + 1][j], matrix[i + 1][j + 1]])
            if matrix[i][j][0]>550 and matrix[i][j][1]<550:
              
                source_coords = np.float32(
                    [(matrix[i][j][0]+10,matrix[i][j][1]-10), matrix[i][j + 1], matrix[i + 1][j], matrix[i + 1][j + 1]])
            if matrix[i][j][0]>550 and matrix[i][j][1]>550:
            
                source_coords = np.float32(
                    [matrix[i][j]+10, matrix[i][j + 1], matrix[i + 1][j], matrix[i + 1][j + 1]])
            dest_coords = np.float32([[0, 0], [125, 0], [0, 125], [125, 125]])
            transform_matrix = cv2.getPerspectiveTransform(
                source_coords, dest_coords)
 
            fields.append([cv2.warpPerspective(img, transform_matrix, (125, 125)), chr(
                65 + i) + str(j+1), source_coords])
 
    return fields
 
def sortIntersections(intersections):
    """ Sortowanie punktow przeciec aby w lewym gornym rogu bylo 0 """
    sorted_intersections = []
    sortedY = sorted(intersections, key= lambda k : k[1])
 
    for i in range (0, 82, 9):
        row = sortedY[0 + i : 9 + i]
        sorted_intersections.extend(sorted(row, key= lambda k : k[0]))
 
    return sorted_intersections
 
def neuronScore(fields, img, model, class_names, id):
    """ Zdjecie wynikowe, okreslajace prawdopodobienstwo jaka bierka stoi na danym polu """
    img2 = img.copy()
    results = []
    for i, x in enumerate(fields):
        start_point = (int(x[2][0][0]), int(x[2][0][1]))
        end_point = (int(x[2][0][0]+125), int(x[2][0][1])+125)
 
        cv2.rectangle(img2, start_point, end_point, (255,255,0), 2)
 
        field = np.expand_dims(x[0], axis = 0)
        predictions_single = model.predict(field)
        score = tf.nn.softmax(predictions_single[0])
        cv2.putText(img2, "{} {:2.0f})".format(class_names[np.argmax(score)],
                        100*np.max(predictions_single)),(start_point[0]+10, start_point[1]+15), cv2.FONT_HERSHEY_SIMPLEX, 0.33,(0, 0, 255),1,cv2.LINE_AA)
 
        results.append(class_names[np.argmax(score)])
 
    copyToClipboard(saveToJsonAndFEN(results, id))
    return img2

def copyToClipboard(string):
    """ Kopiowanie do schowka Windowsego formatu FEN """
    r = Tk()
    r.withdraw()
    r.clipboard_clear()
    r.clipboard_append(string)
    r.update()
    r.destroy()

""" def buildFEN(results, l, licz):
    Konktatenacja stringa FEN-owego 
    string = ""
    if results[l] == "White-King":
        if licz > 0:
            string += str(licz) + "K"
            licz = 0
        else:
            string += "K"
    if results[l] == "Black-King":
        if licz > 0:
            string += str(licz) + "k"
            licz = 0
        else:
            string += "k"
    if results[l] == "White-Queen":
        if licz > 0:
            string += str(licz) + "Q"
            licz = 0
        else:
            string += "Q"
    if results[l] == "Black-Queen":
        if licz > 0:
            string += str(licz) + "q"
            licz = 0
        else:
            string += "q"
    if results[l] == "White-Pawn":
        if licz > 0:
            string += str(licz) + "P"
            licz = 0
        else:
            string += "P"
    if results[l] == "Black-Pawn":
        if licz > 0:
            string += str(licz) + "p"
            licz = 0
        else:
            string += "p"
    if results[l] == "White-Bishop":
        if licz > 0:
            string += str(licz) + "B"
            licz = 0
        else:
            string += "B"
    if results[l] == "Black-Bishop":
        if licz > 0:
            string += str(licz) + "b"
            licz = 0
        else:
            string += "b"
    if results[l] == "White-Rook":
        if licz > 0:
            string += str(licz) + "R"
            licz = 0
        else:
            string += "R"
    if results[l] == "Black-Rook":
        if licz > 0:
            string += str(licz) + "r"
            licz = 0
        else:
            string += "r"
    if results[l] == "White-Knight":
        if licz > 0:
            string += str(licz) + "N"
            licz = 0
        else:
            string += "N"
    if results[l] == "Black-Knight":
        if licz > 0:
            string += str(licz) + "n"
            licz = 0
        else:
            string += "n"
    
    licz = 0
    return string"""

def saveToJsonAndFEN(results, id):
    """ Zapis do pliku json i pomoc w FEN"""
    pole = ['A8','B8','C8','D8','E8','F8','G8','H8','A7','B7','C7','D7','E7','F7','G7','H7','A6','B6','C6','D6','E6','F6','G6','H6','A5','B5','C5','D5','E5','F5','G5','H5'
        ,'A4','B4','C4','D4','E4','F4','G4','H4','A3','B3','C3','D3','E3','F3','G3','H3','A2','B2','C2','D2','E2','F2','G2','H2','A1','B1','C1','D1','E1','F1','G1','H1']
    l=0
    licz=0
    dictionary = {}
    dictionary['pieces'] = []

    string = ""
 
    for i in range(8):
        licz = 0
        for j in range(8):
 
            dictionary['pieces'].append({
                "place ":pole[l],
                "piece ":results[l]
            })
            """ Budowanie formatu FEN """
            #string += buildFEN(results, l, licz)
 
 
            if results[l] == "White-King":
                if licz > 0:
                    string += str(licz)
                    licz = 0
                string += "K"
            elif results[l] == "Black-King":
                if licz > 0:
                    string += str(licz) + "k"
                    licz = 0
                string += "k"
            elif results[l] == "White-Queen":
                if licz > 0:
                    string += str(licz)
                    licz = 0
                string += "Q"
            elif results[l] == "Black-Queen":
                if licz > 0:
                    string += str(licz)
                    licz = 0
                string += "q"
            elif results[l] == "White-Pawn":
                if licz > 0:
                    string += str(licz)
                    licz = 0
                string += "P"
            elif results[l] == "Black-Pawn":
                if licz > 0:
                    string += str(licz)
                    licz = 0
                string += "p"
            elif results[l] == "White-Bishop":
                if licz > 0:
                    string += str(licz)
                    licz = 0
                string += "B"
            elif results[l] == "Black-Bishop":
                if licz > 0:
                    string += str(licz)
                    licz = 0
                string += "b"
            elif results[l] == "White-Rook":
                if licz > 0:
                    string += str(licz)
                    licz = 0
                string += "R"
            elif results[l] == "Black-Rook":
                if licz > 0:
                    string += str(licz)
                    licz = 0
                string += "r"
            elif results[l] == "White-Knight":
                if licz > 0:
                    string += str(licz)
                    licz = 0
                string += "N"
            elif results[l] == "Black-Knight":
                if licz > 0:
                    string += str(licz)
                    licz = 0
                string += "n"
            else:
                licz+=1
 
            l+=1    
            print(string)
 
 
        if licz > 0:
            string+=str(licz)    
        string+='/'
 
 
    string = string[:-1]
    requests.post("http://sleepy-falls-41701.herokuapp.com/AI/add?id=" + id, data= json.dumps(dictionary['pieces']))
    return string


def findPerspectiveTransform(intersections2, img):
    """ Rzut perspektywistyczny """
    drawed_img = img.copy()
    max_sum = 0
    min_sum = 10000
    max_diff = 0
    min_diff = 10000
    for point in intersections2:
        if max_sum < point[0] + point[1]:
            max_sum = point[0] + point[1]
            botright = tuple((point[0], point[1]))
        if min_sum > point[0] + point[1]:
            min_sum = point[0] + point[1]
            topleft = tuple((point[0], point[1]))
        if max_diff < abs(point[0] - point[1]):
            max_diff = point[0] - point[1]
            topright = tuple((point[0], point[1]))
        if min_diff > point[0] - point[1]:
            min_diff = point[0] - point[1]
            botleft = tuple((point[0], point[1]))
 
    pts3 = np.float32([[topright[0], topright[1]],[botright[0], botright[1]],[botleft[0], botleft[1]], [topleft[0], topleft[1]]])
 
    pts4 = np.float32([[0, 0], [1100, 0], [1100, 1100], [0, 1100]])
 
    M4 = cv2.getPerspectiveTransform(pts3, pts4)
 
    cv2.circle(img, botright, 5, (255, 0, 255), 2, cv2.LINE_AA)
    cv2.circle(img, topright, 5, (255, 0, 255), 2, cv2.LINE_AA)
    cv2.circle(img, topleft, 5, (255, 0, 255), 2, cv2.LINE_AA)
    cv2.circle(img, botleft, 5, (255, 0, 255), 2, cv2.LINE_AA)
 
    dst4 = cv2.warpPerspective(drawed_img, M4, (1100, 1100))
    return dst4
 
def getChessboardContours(img):
    """ Segmentacja obrazu, wyciecie tla """
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    mask = np.zeros((gray.shape),np.uint8)
    thresh = cv2.adaptiveThreshold(gray,255,0,1,19,2) 
    contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
 
    maks = 0
    best_cnt = None
    for i in range(0, len(contours)):
        cnt = contours[i]
        area = cv2.contourArea(cnt) 
        if area > maks:
            maks = area
            best_cnt = cnt  
 
    cv2.drawContours(mask,[best_cnt],0,255,-1)
    cv2.drawContours(mask,[best_cnt],0,0,2)
    res = cv2.bitwise_and(img, img, mask=mask)
    return res

def run(name_file, id):
    """ Główna funkcja ML, ładuje model, wywoływane są wszystkie metody pomocnicze """
    model = keras.models.load_model('model/')
    class_names = ['Black-Bishop', 'Black-King', 'Black-Knight', 'Black-Pawn', 'Black-Queen', 'Black-Rook', 'Empty',
    'White-Bishop', 'White-King', 'White-Knight', 'White-Pawn', 'White-Queen', 'White-Rook']
 
    img = cv2.imread(name_file)
    resized = getChessboardContours(img)
    resized = cv2.resize(resized, (1920,1080))
    canny = auto_canny(resized)
 
    lines = cv2.HoughLines(canny, 1, np.pi/180, 170)
    segmented_kmeans = segment_by_angle_kmeans(lines)
    segmented_intersections = segmentedIntersections(segmented_kmeans)
    perspective_image = findPerspectiveTransform(segmented_intersections, resized)
 
    canny_persp = auto_canny(perspective_image)
    lines_persp = cv2.HoughLines(canny_persp, 1, np.pi/180, 170)
    segmented_kmeans_persp = segment_by_angle_kmeans(lines_persp)
    segmented_intersections_persp = segmentedIntersections(segmented_kmeans_persp)
    filter_intersections_persp = filterIntersections(segmented_intersections_persp)
    picked_points_persp = pick_points(filter_intersections_persp)
    sorted_intersections_persp = sortIntersections(picked_points_persp)
    fields = splitChessboardImageIntoFields(sorted_intersections_persp, perspective_image)
 
    img2 = neuronScore(fields, perspective_image, model, class_names, id)
    img2 = cv2.resize(img2, (900,900))
    cv2.imshow('AI Result', img2)
