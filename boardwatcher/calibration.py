import cv2 as cv
import numpy as np
import math

from boardwatcher import gui, lines as l, exception
from boardwatcher.imageProcessor import ImageProcessor
from boardwatcher.camera import Camera
from boardwatcher.exception import ExitAppException
from boardwatcher.exception import AutoCalibrationException
from boardwatcher.chess import GameplayAnalysis
from boardwatcher.painter import Painter

class Calibration:
    """ Klasa realizująca proces kalibracji szachownicy """

    def __init__(self, ip):
        """ Przypisanie wartości początkowych obiektu """
        self.mode = False
        self.index = 0
        self.ix = -1
        self.iy = -1
        self.threshold = 300
        self.persp_threshold = 300
        self.ip = ip
        self.auto_calibration = True
        self.calibrated = False

    def injectGui(self, gui):
        """ Wstrzyknięcie obiektu GUI """
        self.gui = gui
    
    def isReadyToInitGameplay(self):
        """ Informacja, czy obiekt zgłosił gotowość do rozpoczęcia rozgrywki """
        return True if self.isCalibrated and self.ip.hasPickedPoints() else False

    def getChessboardCornerPoints(self, ip):
        """ TODO """
        return ip.rectangle if self.auto_calibration else ip.rectangle

    def getThreshold(self):
        """ Pobranie wartości progowania dla rzutu 2D """
        return self.threshold

    def getPerspThreshold(self):
        """ Pobranie wartości progowania dla klatki pobranej z kamery """
        return self.persp_threshold    
        
    def calibrate(self, driver):
        """ Główna metoda inicjalizująca kalibrowanie oraz kontrolująca jego stan """
        if not self.calibrated and self.auto_calibration:
            try:
                self.autoCalibrate(driver)
            except AutoCalibrationException: pass
            except Exception: pass
        elif not self.calibrated and not self.auto_calibration:
            self.manualCalibrate(driver)

    def decalibrate(self):
        """
        Wycofanie kalibracji poprzez usunięcie informacji z obiektu klasy ImageProcessor o zapisanych
        współrzędnych pól szachownicy oraz zmiana stanu bieżącego obiektu
        """
        self.ip.removePicked()
        self.calibrated = False
    
    def isCalibrated(self):
        """ Informacja, czy szachownica jest poprawnie wykrywana przez program """
        return self.calibrated

    def isManualCalibrationMode(self):
        """ Informacja czy program działa w manualnym trybie wskazania położenia narożników szachownicy """
        return True if not self.auto_calibration else False

    def revertCalibration(self, cam):
        """ Wycofanie kalibracji """
        self.calibrated = False
        Painter.drawNoChessboard("Gameplay", cam)

    def calibratePerspThreshold(self, factor):
        """ Ustawienie poziomu progowania dla klatki pobranej z kamery """
        if self.threshold + factor >= 30:
            self.persp_threshold = self.persp_threshold + factor
            gui.setTrackbarPos('PerspThreshold', self.persp_threshold)
            self.gui.perspective_threshold_slider.set(self.persp_threshold)

    def calibrateThreshold(self, factor):
        """ Ustawienie poziomu progowania dla rzutu 2D """
        if self.threshold + factor >= 30:
            self.threshold = self.threshold + factor
            gui.setTrackbarPos('Threshold', self.threshold)
            self.gui.threshold_slider.set(self.threshold)

    def calculateThresholdFactor(self, lines_count):
        """ Obliczenie współczynnika progowania dla automatyczniej kalibracji """
        if lines_count >= 0 and lines_count < 5:
            return -20
        elif lines_count >= 5 and lines_count < 30:
            return -1
        elif 30 >= lines_count < 50:
            return 1
        elif 20 <= lines_count < 100:
            return 1
        elif 100 <= lines_count < 200:
            return 2
        elif 200 <= lines_count < 1000:
            return 100
        elif 1000 <= lines_count:
            return 150
        else:
            return 1

    def analyzePreview(self, lines):
        """ Dynamiczne zarządzanie progowaniem próbkowanego obrazu z kamery bazując na bieżącej liczbie wykrytych linii """
        lines_count = len(lines)
        factor = self.calculateThresholdFactor(lines_count)
        self.calibratePerspThreshold(factor)

    def analyzeGameplay(self, items):
        """ Dynamiczne zarządzanie progowaniem rzutu 2D bazując na bieżącej liczbie wykrytych linii """
        items_count = len(items)
        factor = self.calculateThresholdFactor(items_count)
        self.calibrateThreshold(factor - 5)

    def autoCalibrate(self, driver):
        """
        Dwuetapowe bezobsługowe poszukiwanie narożników szachownicy
        a następnie 81 współrzędnych przecięć pól szachownicy
        """
        persp = self.calibrate3D(driver)
        self.calibrate2D(driver, persp)

    def calibrate2D(self, driver, preview):
        """
        Szukanie linii w rzucie 2D, pozostawienie unikalnych linii zbliżonych do kątów prostopadłych do krawędzi okna,
        wywołanie zarządzania progowaniem jeżeli proces kalibracji nie przebiegł pomyślnie
        zatwierdzenie współrzędnych przecięć pól w obiekcie klasy ImageProcessor w przeciwnym przypadku
        """
        if not self.ip.hasChessboardCorners():
            return
        img = self.ip.getPerspectiveTransform(preview)
        driver.drawDebugImg("Gameplay", img)
        driver.imshow("Preview", preview, -1200, 200)
        lines = l.getLines(img, driver.cal.threshold)
        self.analyzeGameplay(lines)
        if len(lines) > 500:
            return
        if driver.perpendicular:
            lines, perpendicular_lines, additional_lines = l.extractPerpendicular(lines, driver)
        intersections = l.getIntersectionsByLines(lines)
        self.ip.picked = self.ip.pick_points(intersections) if len(intersections) > 0 else []
        Painter.drawNoChessboard("Gameplay", driver.cam)
        if self.ip.notEnoughtIntersections():
            self.analyzeGameplay(self.ip.picked)
            return
        self.ip.sortIntersections()
        self.calibrated = True
        # gui.setTrackbarPos('Calibrated', 1)
        self.ip.corners = self.ip.getImgCornerRects(preview, driver.cam)
    
    def calibrate3D(self, driver):
        """
        Pobranie próbki obrazu z kamery,
        wywołanie procesu konturowania na próbca,
        pobranie linii z potencjalnie namierzonej szachownicy,
        zapisanie informacji o WSPÓŁRZĘDNYCH narożników szachownicy w obiekcie ImageProcessor jeżeli proces kalibracji przebiegł pomyślnie
        """
        persp = driver.cam.sampleCameraFrame()
        preview = persp.copy()
        persp = self.ip.getChessboardContours(persp)
        Painter.drawAutoCalibrationInProgress("Preview", preview)
        lines = l.getLines(persp, self.persp_threshold)
        self.analyzePreview(lines)
        if len(lines) > 1000:
            return
        if lines is not None and len(lines) < 100:
            l.drawLines(lines, persp, driver, (100, 100, 255), 1)
        if len(lines) < 14:
            return
        driver.drawDebugImg("Persp", persp)
        self.ip.setChessboardCornersByLinesLimitedByCameraResolution(lines, driver.cam.getResolution())
        Painter.drawPoints(self.ip.rectangle, persp)

        return preview

    def manualCalibrate(self, driver):
        """
        Rysowanie aktualnie wyznaczonych przez użytkownika punktów narożnych szachownicy w trybie manualnym
        oraz ustawnienie stanu kalibracji na skalibrowany jeżeli wyznaczono 4 punkty
        """
        persp = driver.cam.sampleCameraFrame()
        Painter.drawManualCalibrationInProgress("Preview", persp)

        for i in range(0, len(self.ip.rectangle)):
            cv.circle(persp, (self.ip.rectangle[i][0], self.ip.rectangle[i][1]), 5, (255, 255, 255), 2)

        cv.imshow("Preview", persp)
        if len(self.ip.rectangle) == 4:
            self.calibrated = True

    def manualCalibrationMouseCallback(self,event, x, y, flags, ip):
        """
        Metoda Callback do manualnego zaznaczania narożników szachownicy
        w oknie 'Preview' za pomocą myszy
        """
        if event == cv.EVENT_LBUTTONDOWN:
            if len(ip.rectangle) == 0:
                ip.rectangle.append([x, y])

            if len(ip.rectangle) >= 1:
                licz = 0
                for i in range(0, len(ip.rectangle)):
                    dl = math.sqrt(((ip.rectangle[i][0]-x) ** 2) + ((ip.rectangle[i][1]-y) ** 2))
                    print(dl)
                    # jezeli dlugosci miedzy wszystkimi zaznaczonymi punktami są większe od 15 to zwiększ licznik o 1
                    if dl > 15 and len(ip.rectangle) < 4:
                        licz += 1
                    elif dl < 25:
                        self.mode = True
                        self.index = i

                if licz == len(ip.rectangle):
                    ip.rectangle.append([x, y])

                licz = 0

        if event == cv.EVENT_LBUTTONUP:
            if self.mode == True:
                ip.rectangle.remove(ip.rectangle[self.index])
                ip.rectangle.append([self.ix, self.iy])
                self.mode = False

        if event == cv.EVENT_MOUSEMOVE:
            self.ix, self.iy = x, y

        if event == cv.EVENT_RBUTTONDOWN:
            for i in range(0, len(ip.rectangle)):
                dl = math.sqrt(((ip.rectangle[i][0]-x) ** 2) + ((ip.rectangle[i][1]-y) ** 2))
                if dl < 20:
                    ip.rectangle.remove(ip.rectangle[i])
                    break

