import cv2 as cv
import numpy as np
import boardwatcher.lines as l
from sys import exit
from boardwatcher.exception import AutoCalibrationException
class Camera:
    """
    Klasa zarządzająca stanem kamery
    oraz oświetlenia sceny
    """

    def __init__(self):
        """
        Ustawienie domyślnych wartości obiektu,
        podpięcie kamery, wymuszenie rozdzielczości HD
        ( domyślnie biblioteka OpenCV ignorowała natywną rozdzielczość kamery 
        i uruchamiała program w rozdzielczości 640x480)
        """
        self.rectangle = None
        self.cam = cv.VideoCapture(0)
        # if self.cam is None or not self.cam.isOpened():
            # exit("No Cam connected")


        # manualne ustawienie rozdzielczosci
        self.cam.set(cv.CAP_PROP_FRAME_WIDTH, 1280)
        self.cam.set(cv.CAP_PROP_FRAME_HEIGHT, 720)

        self.res_y, self.res_x, self.bits = self.cam.read()[1].shape

        # 40 is gone because of imageProcessor.py - line 93 - getPerspectiveTransform()
        self.width2D = 760 # soon responsible self.res_x
        self.height2D = 760 # this one too self.res_x

        self.exposure = self.__guessProperExposure()
        self.cam.set(self.exposure, 0.25)
        self.exposed = False

    def __guessProperExposure(self):
        """ Ustalenie wyjściowego współczynnika jasności sceny """
        limit = 10
        mean = 0
        for i in range(0, limit):
            persp = self.sampleSubCameraFrame()
            mean = np.mean(persp) + mean
        mean = mean / limit
        if mean < 120 and mean > 70:
            result = 3
        elif mean <= 70:
            result = 5
        elif mean >= 120:
            result = -3

        return result

    def sampleSubCameraFrame(self):
        """
        Próbkowanie kamery z obrazu pomniejszonej o margines z każdej krawędzi
        o mnożnik zawarty w zmiennej 'factor'
        """
        img = self.sampleCameraFrame()
        blank = np.zeros(self.getResolution(), np.uint8)

        factor = 0.2137
        # return blank
        minX = int((self.res_x/2) - (factor * self.res_x))
        maxX = int((self.res_x/2) + (factor * self.res_x))
        minY = int((self.res_y/2) - (factor * self.res_y))
        maxY = int((self.res_y/2) + (factor * self.res_y))
        return img[minY : maxY, minX : maxX]

    def sampleCameraFrame(self):
        """ Pobranie próbki obrazu z kamery """
        _, img = self.cam.read()

        return img

    def getResolution(self):
        """ Pobranie rozdzielczości kamery """
        return (self.res_y, self.res_x, self.bits)
    
    def hasProperLit(self):
        """ Informacja, czy oświetlenie sceny nie zmieniło się """
        return True if self.exposed else False

    def get2DResolution(self):
        """ Pobranie ustalonego rozmiaru płótna rzutu 2D """
        return (self.width2D, self.height2D, self.bits)
    
    def getTextRenderPos(self):
        """ Pobranie współrzędnych centralnych dla bieżącej rozdzielczości kamery """
        return (int(self.width2D/2 - 50), int(self.height2D/2 - 10))
    
    # def autoExposure(self, img):
    #     """ Dynamiczne ustalenie poziomu otwarcia przesłony kamery """
    #     mean = np.mean(img)
    #     if mean < 120 and mean > 70:
    #         self.exposed = True
    #         return True
    #     if mean <= 70:
    #         self.exposure = float(self.exposure - 0.1)
    #     elif mean >= 120:
    #         self.exposure = float(self.exposure + 0.1)

    #     self.set(cv.CAP_PROP_EXPOSURE, self.exposure)

    def autoExposure2(self, img):
        mean = np.mean(img)
        if mean < 160 and mean > 80:
            self.exposed = True
            return True
        if mean <= 80:
            self.exposure = float(self.exposure - 0.1)
        elif mean >= 160: #and mean < 180:
            self.exposure = float(self.exposure + 0.1)
        # else:
            # self.exposure = 0

        self.cam.set(cv.CAP_PROP_EXPOSURE, self.exposure)
    
    def litHasChanged(self):
        return self.exposed

    def autoExposure(self, driver):
        persp = driver.cam.sampleCameraFrame()
        subpersp = driver.cam.sampleSubCameraFrame()
        lines = l.getLines(persp, driver.cal.persp_threshold)
        sublines = l.getLines(subpersp, driver.cal.persp_threshold - 21)
        if lines is not None:
            l.drawLines(lines, persp, driver, (100, 100, 255), 1)

        driver.imshow("Preview", persp, -1200, 200)
        self.autoExposure2(subpersp)

    def setupLit(self, driver):
        if not self.hasProperLit():
            try:
                self.autoExposure(driver)
            except AutoCalibrationException:
                driver.cal.calibratePerspThreshold(driver.gui)
            except Exception: pass
                # print("Too many lines in autoexposure")
    
    def illumChanged(self, persp):
        """ Informacja, czy """
        return False