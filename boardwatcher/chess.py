import cv2 as cv
import numpy as np
import string
import random
import requests
import json
from boardwatcher.painter import Painter
from boardwatcher.pieceMove import PieceMove

class GameplayAnalysis(PieceMove):
    """
    Klasa zarządzająca przebiegiem rozgrywki, rozszerza klasę obsługującą ruch bierek
    """
    # Game states
    INITIAL = 0
    STANDBY = 1
    REVERT_MOVE = 2
    MADE_A_MOVE = 3
    

    gamestate = INITIAL

    # PGN actions
    EMPTY_FIELD_MOVE = 0
    CAPTURE = 1
    KING_SIDE_CASTLE = 1
    QUEEN_SIDE_CASTLE = 0

    def __init__(self):
        self.gameplay = []
        self.fields = []
        self.means = []
        self.pgn_moves = []
        self.gamestate = self.INITIAL
        self.id = '' . join(random.choices(string.ascii_letters + string.digits, k = 5))
        requests.post("http://sleepy-falls-41701.herokuapp.com/start?id=" + self.id)
        print("GameID: ", self.id)
    
    def getGameId(self):
        """ Zwrócenie ID gry niezbędne do skorzystania z poglądu partii w aplikacji Board Watcher Client Webapp """
        return self.id

    def gameplayHasInitialState(self):
        """ Informacja, czy rozgrywka jest w stanie początkowym, przed wykonaniem pierwszego ruchu """
        return True if self.gamestate == self.INITIAL else False

    def moveHasBeenTaken(self):
        """ Informacja, czy został wykonany ruch """
        return True if self.gamestate == self.MADE_A_MOVE else False
    
    def setGameplayStateToStandby(self):
        """ Ustawnienie stanu rozgrywki na oczekujący na zatwierdzenie kolejnego ruchu """
        self.gamestate = self.STANDBY

    def setGameplayStateToMadeMove(self):
        """ Ustawnienie stanu rozgrywki na wykonano ruch """
        self.gamestate = self.MADE_A_MOVE

    def __drawByCoors(self, img, field, color = (0, 255, 0)):
        """ Rysowanie obramowania wokół podanego pola """
        cv.rectangle(img, (int(field[self.LEFT_TOP][self.X]), int(field[self.LEFT_TOP][self.Y])), (int(field[self.RIGHT_BOTTOM][self.X]), int(field[self.RIGHT_BOTTOM][self.Y])), color, 5)

    def __calcMeanThresholdOfAllFields(self, previous, current):
        """ Liczenie średniej wartości pikseli dla wszystkich zapisanych pól """
        mean_table = []
        for x, y in zip(previous, current):
            mean_table.append(self._getMeanOfTwoFields(x, y))
        
        threshold = sum(mean_table) / len(mean_table)

        return threshold

    def reset(self):
        """ Zresetowanie stanu rozgrywki do pozycji wyjściowej """
        self.fields = []
        self.gameplay = []
        self.pgn_moves = []
        self.gamestate = self.INITIAL

    def setGameplayStateToRevert(self):
        """ Ustawnienie stanu rozgrywki na wycofanie najnowszego zatwierdzonego ruchu """
        self.gamestate = self.REVERT_MOVE

    def revertMove(self, curr_fields):
        """ Wykonanie akcji wycofania najnowszego zatwierdzonego ruchu """
        if len(self.pgn_moves) == 0 or len(self.gameplay) == 0:
            print("Nothing to revert")
            self.setGameplayStateToStandby()
            return
        print("Revert")
        self.pgn_moves.pop()
        self.gameplay.pop()
        prev_fields = self.gameplay.pop()
        curr_fields = self.__appendInfoFromPreviousFieldsToCurrentFields(prev_fields, curr_fields)
        self.gameplay.append(curr_fields)
        self.fields = curr_fields
        self.setGameplayStateToStandby()
        requests.get("http://sleepy-falls-41701.herokuapp.com/revert?id=" + self.id);

    def setInitPosition(self, fields):
        """ Ustawienie stanu obiektu analizy rozgrywki na gotowy do rozpoczęcia gry """
        # is taken
        for i in range(len(fields)):
            fields[i].append(True) if self.isStartupField(i) else fields[i].append(False)
        # piece name
        for i in range(len(fields)):
            name = self.getPieceNameByIndexInInitPosition(i)
            fields[i].append(name)
        # field owner
        for i in range(len(fields)):
            owner = self.getFieldOwnerByIndexInInitPosition(i)
            fields[i].append(owner)
        self.gameplay.append(fields)
        self.fields = fields
        self.ON_THE_MOVE = self.WHITE
        self.setGameplayStateToStandby()

    def drawCurrentPosition(self, img):
        """ Zaznaczenie w oknie 'Gameplay' pól okupowanych przez bierki kolorem zgodym z właścicielem pola """
        if self.fields is None:
            return
        for x in self.fields:
            if self.fieldHeldByWhite(x):
                self.__drawByCoors(img, x[self.COORDS], (255, 255, 255))
            elif self.fieldHeldByBlack(x):
                self.__drawByCoors(img, x[self.COORDS], (0, 0, 0))
        
    def getCurrentPlayer(self):
        """ Pobranie informacji o graczu na posunięciu """
        return "WHITE" if len(self.gameplay) %2 == 1 else "BLACK"

    def getNextPlayer(self):
        """ Pobranie informacji o graczu, którego kolej jest w następnym ruchu """
        return "WHITE" if len(self.gameplay) %2 == 0 else "BLACK"

    def makeMove(self, current_fields_set):
        """
        WAŻNE \n
        Złożona metoda wykonywująca niezbędne działania związane z poprawnym przetworzeniem
        aktualnego stanu rozgrywki po zatwierdzeniu ruchu
        oraz wywołująca szereg metod pomocniczych realizujących podzadania
        Na tym etapie wysyłana jest informacja do klienta webowego, że został wykonany nowy ruch
        """
        if self.gameplay is None or len(self.gameplay) == 0:
            print("WARNING - NO PREVIOUS MOVE'S")
            self.gamestate = self.STANDBY
            return
        curr = current_fields_set
        previous = self.gameplay[-1]
        mean_threshold = self.__calcMeanThresholdOfAllFields(previous, curr)
        if(mean_threshold > 20):
            self.reset()
            print("Lit changed")
            self.gamestate = self.STANDBY
            return
        current_player = self.getCurrentPlayer()
        #move debug start point
        changed = self._getChangedFields(curr, previous, mean_threshold)
        changed = self._filterChangedFields(changed, current_player)
        print ([self._getFieldNameByIndex(x[0][0]) for x in changed])
        is_castle = False

        if len(changed) == 0 or len(changed) == 1 or changed[0][1] == False:
            print("Incorrect move, undo and try again")
            self.gamestate = self.STANDBY
            return

        if self.__castleHasTaken(changed):
            is_castle = True
            print("CASTLE")
        else:
            origin = changed[0][0][0]
            origin_field = changed[0][2]
            # start - wypadałoby to przerobić na jakąś pętlę sprawdzającą niż wzięcie po prostu kolejnego pola, mało eleganckie i może się wygenerować przypadek, w którym nie zadziała
            destination = changed[1][0][0] if not changed[1][1] else changed[2][0][0] # jeżeli drugie z kolei pole należało do obecnego to weź kolejne
            destination_field = changed[1][2] if not changed[1][1] else changed[2][2]
            # end
        curr = self.__appendInfoFromPreviousFieldsToCurrentFields(previous, curr)

        if is_castle:
            # PGN append inside __performCastle()
            curr = self.__performCastle(previous, curr, changed)
        elif self.__isCaptured(previous, origin, destination): # capture
            self.__appendPGNMove(self.CAPTURE, origin_field, destination_field)
            curr = self.__capture(previous, curr, current_player, origin, destination)
            move = [{"piece": origin_field[4], \
                     "from": origin_field[1],  \
                     "to": destination_field[1], \
                     "color": False if origin_field[5] == "WHITE" else True}]
            requests.post("http://sleepy-falls-41701.herokuapp.com/move?id=" + self.id, data=json.dumps(move))
        elif self.__isMovedToEmptyField(previous, origin, destination):
            self.__appendPGNMove(self.EMPTY_FIELD_MOVE, origin_field, destination_field)
            curr = self.__moveToEmptyField(previous, curr, current_player, origin, destination)
            move = [{"piece": origin_field[4], \
                     "from": origin_field[1],  \
                     "to": destination_field[1], \
                     "color": False if origin_field[5] == "WHITE" else True}]
            requests.post("http://sleepy-falls-41701.herokuapp.com/move?id=" + self.id, data=json.dumps(move))

        self.gameplay.append(curr)
        self.fields = curr
        self.gamestate = self.STANDBY

        print("PGN")
        print([move for move in self.pgn_moves])


    def __appendPGNMove(self, flag, origin_field = None, destination_field = None):
        """ Dopisanie do obiektu przebiegu rozgrywki kolejnego zatwierdzonego ruchu w notacji PGN ( oprócz roszad ) """
        FIRST_LETTER = 0
        SECOND_LETTER = 1
        if origin_field[self.PIECE_NAME] == self.KNIGHT: # KING - KNIGHT collision
            piece_symbol = origin_field[self.PIECE_NAME][SECOND_LETTER]
        else:
            piece_symbol = origin_field[self.PIECE_NAME][FIRST_LETTER]

        destination = str(destination_field[self.FIELD_NAME]).lower()

        if piece_symbol == "P":
            origin = str(origin_field[self.FIELD_NAME][FIRST_LETTER]).lower()
        else:
            origin = origin_field[self.PIECE_NAME][FIRST_LETTER]

        if flag == self.EMPTY_FIELD_MOVE:
            if piece_symbol == "P":
                move = destination
            else:
                move = piece_symbol + destination
        elif flag == self.CAPTURE:
            if piece_symbol == "P":
                move = origin + "x" + destination
            else:
                move = piece_symbol + "x" + destination

        self.pgn_moves.append(move)
    
    def __appendCastlePGNMove(self, is_king_side_castle):
        """ Dopisanie do obiektu przebiegu rozgrywki zatwierdzonej roszadyw notacji PGN """
        if is_king_side_castle:
            move = "O-O"
        else:
            move = "O-O-O"

        self.pgn_moves.append(move)

    def __isCaptured(self, previous, origin, destination):
        """ Infomacja, czy zostało zagrane bicie """
        return True if previous[origin][self.IS_TAKEN] == True and previous[destination][self.IS_TAKEN] == True else False

    def __isMovedToEmptyField(self, previous, a, b):
        """ Informacja, czy bierka jest zagrana na puste pole """
        return True if (previous[a][self.IS_TAKEN] == True and previous[b][self.IS_TAKEN] == False) or (previous[a][self.IS_TAKEN] == False and previous[b][self.IS_TAKEN] == True) else False

    def __capture(self, previous, curr, current_player, a, b):
        if previous[a][self.FIELD_OWNER] == current_player:
            curr[a][self.FIELD_OWNER] = self.NEUTRAL
            curr[a][self.IS_TAKEN] = False
            curr[a][self.PIECE_NAME] = self.EMPTY
            curr[b][self.FIELD_OWNER] = current_player
            curr[b][self.PIECE_NAME] = previous[a][self.PIECE_NAME]
        else:
            curr[a][self.FIELD_OWNER] = current_player
            curr[a][self.PIECE_NAME] = previous[b][self.PIECE_NAME]
            curr[b][self.PIECE_NAME] = self.EMPTY
            curr[b][self.IS_TAKEN] = False
            curr[b][self.FIELD_OWNER] = self.NEUTRAL
    
        return curr
    
    def __moveToEmptyField(self, previous, curr, current_player, a, b):
        """ Wykonanie akcji przeniesienia bierki na puste pole i zapisanie tej informacji w obiekcie przebiegu rozgrywki """
        curr[a][self.FIELD_OWNER], curr[b][self.FIELD_OWNER] = curr[b][self.FIELD_OWNER], curr[a][self.FIELD_OWNER]
        curr[a][self.IS_TAKEN], curr[b][self.IS_TAKEN] = curr[b][self.IS_TAKEN], curr[a][self.IS_TAKEN]
        curr[a][self.PIECE_NAME], curr[b][self.PIECE_NAME] = curr[b][self.PIECE_NAME], curr[a][self.PIECE_NAME]  

        return curr

    def __castleHasTaken(self, changed):
        """ Informacja, czy została wykonana roszada """
        # four fields is needed to change if castling
        if len(changed) < 3: # powinno być 4, ale raz mi nie złapało G1 jako zmienione więc zwiększam tolerancję
            return False
        # two fields of current player needed to cast
        if sum(x[1] for x in changed) != 2: 
            return False
        # two or 3 empty fields of current player needed to cast
        if sum(x[2][self.FIELD_OWNER] == self.NEUTRAL for x in changed) < 2 or sum(x[2][self.FIELD_OWNER] == self.NEUTRAL for x in changed) > 3: 
            return False
        if not any([x[2][self.PIECE_NAME] == self.KING for x in changed]):
            return False
        if not any([x[2][self.PIECE_NAME] == self.ROOK for x in changed]):
            return False
        
        return True

    def __performCastle(self, previous, curr, changed):
        """ Wykonanie roszady i zapisanie informacji w obiekcie przebiegu rozgrywki """
        changed = self.__orderFieldsForCastlePurpose(changed)
        
        if self.__isWhiteKingSideCastling(changed):
            curr = self.__swapCastlingPairs(curr, self.WHITE_KING_STARTUP_POS, 1)
            curr = self.__swapCastlingPairs(curr, self.WHITE_ROOK_KING_SIDE_STARTUP_POS, 2)
            requests.post("http://sleepy-falls-41701.herokuapp.com/move?id=" + self.id, data=json.dumps([
                {'piece':"ROOK", 'from':"H1","to":"F1", "color":False},
                {'piece':"KING", 'from':"E1","to":"G1", "color":False}
            ]))
            self.__appendCastlePGNMove(self.KING_SIDE_CASTLE)
        elif self.__isWhiteQueenSideCastling(changed):
            curr = self.__swapCastlingPairs(curr, self.WHITE_KING_STARTUP_POS, 5)
            curr = self.__swapCastlingPairs(curr, self.WHITE_ROOK_QUEEN_SIDE_STARTUP_POS, 4)
            requests.post("http://sleepy-falls-41701.herokuapp.com/move?id=" + self.id, data=json.dumps([
                {'piece':"ROOK", 'from':"A1","to":"D1", "color":False},
                {'piece':"KING", 'from':"E1","to":"C1", "color":False}
            ]))
            self.__appendCastlePGNMove(self.QUEEN_SIDE_CASTLE)
        elif self.__isBlackKingSideCastling(changed):
            curr = self.__swapCastlingPairs(curr, self.BLACK_KING_STARTUP_POS, 57)
            curr = self.__swapCastlingPairs(curr, self.BLACK_ROOK_KING_SIDE_STARTUP_POS, 58)
            requests.post("http://sleepy-falls-41701.herokuapp.com/move?id=" + self.id, data=json.dumps([
                {'piece':"ROOK", 'from':"H8","to":"F8", "color":True},
                {'piece':"KING", 'from':"E8","to":"G8", "color":True}
            ]))
            self.__appendCastlePGNMove(self.KING_SIDE_CASTLE)
        elif self.__isBlackQueenSideCastling(changed):
            curr = self.__swapCastlingPairs(curr, self.BLACK_KING_STARTUP_POS, 61)
            curr = self.__swapCastlingPairs(curr, self.BLACK_ROOK_QUEEN_SIDE_STARTUP_POS, 60)
            requests.post("http://sleepy-falls-41701.herokuapp.com/move?id=" + self.id, data=json.dumps([
                {'piece':"ROOK", 'from':"A8","to":"D8", "color":True},
                {'piece':"KING", 'from':"E8","to":"C8", "color":True}
            ]))
            self.__appendCastlePGNMove(self.QUEEN_SIDE_CASTLE)
        else:
            "That statement should never appear"

        return curr
    
    def __swapCastlingPairs(self, curr, origin, destination):
        """ Zamienienie miejscami pól """
        # a, b = b, a | well, it does work but actually no
        curr[origin][self.IS_TAKEN], curr[destination][self.IS_TAKEN] = curr[destination][self.IS_TAKEN], curr[origin][self.IS_TAKEN]
        curr[origin][self.PIECE_NAME], curr[destination][self.PIECE_NAME] = curr[destination][self.PIECE_NAME], curr[origin][self.PIECE_NAME]
        curr[origin][self.FIELD_OWNER], curr[destination][self.FIELD_OWNER] = curr[destination][self.FIELD_OWNER], curr[origin][self.FIELD_OWNER]

        return curr

    def __isWhiteKingSideCastling(self, changed):
        """ Informacja, czy została zagrana roszada na skrzydle królewskim przez gracza koloru białego """
        return True if changed[0][0][0] == self.WHITE_KING_STARTUP_POS and changed[1][0][0] == self.WHITE_ROOK_KING_SIDE_STARTUP_POS else False

    def __isWhiteQueenSideCastling(self, changed):
        """ Informacja, czy została zagrana roszada na skrzydle hetmańskim przez gracza koloru białego """
        return True if changed[0][0][0] == self.WHITE_KING_STARTUP_POS and changed[1][0][0] == self.WHITE_ROOK_QUEEN_SIDE_STARTUP_POS else False

    def __isBlackKingSideCastling(self, changed):
        """ Informacja, czy została zagrana roszada na skrzydle królewskim przez gracza koloru czarnego """
        return True if changed[0][0][0] == self.BLACK_KING_STARTUP_POS and changed[1][0][0] == self.BLACK_ROOK_KING_SIDE_STARTUP_POS else False

    def __isBlackQueenSideCastling(self, changed):
        """ Informacja, czy została zagrana roszada na skrzydle hetmańskim przez gracza koloru czarnego """
        return True if changed[0][0][0] == self.BLACK_KING_STARTUP_POS and changed[1][0][0] == self.BLACK_ROOK_QUEEN_SIDE_STARTUP_POS else False

    def __orderFieldsForCastlePurpose(self, changed):
        """
        Sortowanie pól związanych z roszadą w kolejności \n
        0 - KING \n
        1 - ROOK \n
        >1 - EMPTY \n
        """
        # 0 - KING, 1 - ROOK, > 1 - EMPTY
        for i, x in enumerate(changed):
            if x[2][self.PIECE_NAME] == self.KING:
                if i == 0:
                    break
                changed[0], changed[i] = changed[i], changed[0]
                break
        for i, x in enumerate(changed):
            if x[2][self.PIECE_NAME] == self.ROOK:
                if i == 1:
                    break
                changed[1], changed[i] = changed[i], changed[1]
                break
        
        return changed

    def __appendInfoFromPreviousFieldsToCurrentFields(self, previous, curr):
        """ Dopisanie informacji z pól pobranych po zatwierdzeniu poprzedniego ruchu do aktualnie wykrytych pól """
        for i in range(len(curr)):
            curr[i].append(previous[i][self.IS_TAKEN])
            curr[i].append(previous[i][self.PIECE_NAME])
            curr[i].append(previous[i][self.FIELD_OWNER])
        
        return curr

    def drawByFieldName(self, img, name):
        """ Rysowanie obramówki pola po nazwie podanej jako argument wywołania """
        field = self.getFieldCoordsByName(name)
        if field is not None:
            self.__drawByCoors(img, field)

    def gameplayListener(self,event, x, y, flags, param):
        """ Metoda callback wywoływana w momencie interakcji myszy z oknem 'Gameplay' """
        if event == cv.EVENT_MOUSEMOVE:
            return
        if event == cv.EVENT_RBUTTONDOWN:
            print("+-------+")
            print("x: ", x)
            print("y: ", y)
            return

        if event != cv.EVENT_LBUTTONDOWN:
            return
        field = self.getFieldByCoords(x, y)
        if field is None:
            print("Out of Chessboard")
            return
        print("+-------+")
        print("Field name: ", field[self.FIELD_NAME], sep='')
        print("Is Taken: ", field[self.IS_TAKEN], sep='')
        print("Piece name: ", field[self.PIECE_NAME], sep='')
        print("Piece owner: ", field[self.FIELD_OWNER], sep='')
        self.printAvailableMoves(field)
    
    def printAvailableMoves(self, field):
        """ Metoda do debugowania, wypisanie listy dostępnych ruchów """
        print("Available moves: ", end = "")
        available_moves = self._getAvailableMovesByField(field)
        if available_moves is None or len(available_moves) == 0:
            print ("None")
        else:
            for x in available_moves:
                print(x, end = " ")
            print("")

    def vectorToMatrix(self):
        """
        Konwersja bieżących pól szachownicy, które są zapisane w wektorze do 0 do 63
        na macierz 8x8
        """
        res = [[[] for i in range(8)] for j in range(8)]
        for i, x in enumerate(self.fields):
            res[int(i/8)][i%8] = x

        return res

    def getPGNMoves(self):
        """ Pobranie informacji w zapisanych ruchach w formacie PGN """
        return self.pgn_moves
    
    def moveIsReverted(self):
        """ Informacja, czy ruch został wycofany """
        return True if self.gamestate == self.REVERT_MOVE else False

    def updateFieldsWithNewLit(self, fields): pass