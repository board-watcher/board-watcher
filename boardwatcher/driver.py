import cv2 as cv
import numpy as np
import math
 
from boardwatcher import gui as guiDebug, lines as l, exception
from boardwatcher.imageProcessor import ImageProcessor
from boardwatcher.camera import Camera
from boardwatcher.exception import ExitAppException
from boardwatcher.exception import AutoCalibrationException
from boardwatcher.chess import GameplayAnalysis
from boardwatcher.painter import Painter
from boardwatcher.gui import GUI
from boardwatcher.calibration import Calibration
# from boardwatcher.machineLearning import AI
 
class Driver:
    """
    Główna klasa programu. Odpowienik 'int main()'
    Inicjalizacja pozostałych obiektów
    """
 
    def start(self):
        """ Uruchomienie oprogramowania Board Watcher """
        self.init()
        try:
            self.run()
        except ExitAppException:
            cv.destroyAllWindows()
            return
 
    def init(self):
        """ Inicjalizacja niezbędnych pól oraz zależności """
        self.line_mode = 1
        self.current_line = 0
        self.intersection_vis = False
        self.line_filter = True
        self.perpendicular = True
        self.generic = 45
        self.img = None
        self.rectangle = None
        self.corners = None
        self.ip = ImageProcessor()
        self.cal = Calibration(self.ip)
        self.chess = GameplayAnalysis()
        self.game_id = self.chess.getGameId()
        self.cam = Camera()
        self.gui = GUI(self.game_id, self.cal, self.ip, self.chess, self)
        self.cal.injectGui(self.gui)
        self.idle = True
        self.debug = False

        if self.debug:
            guiDebug.initGui(self)
            self.setupDebugGameplayImgCallbacks()
        self.setupPreviewImgCallbacks()
        Painter.drawNoChessboard("Gameplay", self.cam)
 
    def hasIdleState(self):
        """
        Informacja czy program jest w stanie oczekiwania
        na uruchomienie wyszukiwania szachownicy
        i przygotowanie programu do śledzenia rozgrywki """
        return True if self.idle else False
    
    def activate(self):
        """ Rozpoczęcie działania mechanik programu, używane z poziomu przycisku klawiatury lub z poziomu GUI """
        self.idle = False

    def resetGameplay(self):
        """ Wyzerowanie zapisanych ruchów, zresetowanie ustawienia bierek do pozycji początkowej, wymuszenie ponownej kalibracji """
        self.cal.revertCalibration(self.cam)
        self.ip.removeChessboardCornerPoints()
        self.ip.removePicked()
        self.chess.reset()
        self.gui.resetPGN()
 
 
    def drawDebugImg(self, winname, img):
        """ Rysowanie dodatkowych obrazów w trybie debugowania """
        if self.debug:
            self.imshow(winname, img)
 
    def imshow(self, name, img, x = -1200, y = 200):
        """ Rysowanie okien z możliwością wymuszenia pozycji na ekranie ( wyłączone ) """
        cv.namedWindow(name)
        # cv.moveWindow(name, x, y)
        cv.imshow(name, img)
 
    def validateLit(self, persp):
        """ Kontrola stanu oświetlenia sceny """
        if self.cam.illumChanged(persp):
            self.cam.exposed = False
    
    def handleListener(self):
        """ Wywołanie funkcji sprawdzającej, czy nie został wykonany  """
        exception.keyboardListener(self.ip, self.chess, self, self.cal)
 
    def validateCalibration(self, persp):
        """ Kontrola stanu położenia szachownicy w zależności od automatycznego lub manualnego trybu kalibrowania """
        if self.cal.isManualCalibrationMode():
            if not self.cal.isReadyToInitGameplay():
                self.cal.revertCalibration(self.cam)
                self.cal.calibrate2D(self, persp)
        if self.chessboardWasMovedDuringPlay(persp):
            self.cal.revertCalibration(self.cam)
 
    def chessboardWasMovedDuringPlay(self, persp):
        """ Informacja czy szachownica została przesunięta """
        return True if self.cal.isCalibrated() and self.ip.chessboardMoved(persp, self.cam) else False
 
    def setupDebugGameplayImgCallbacks(self):
        """ Funkcja do debugowania stanu pól szachownicy """
        cv.setMouseCallback("Gameplay", self.chess.gameplayListener)

    def setupPreviewImgCallbacks(self):
        """ Ustawnienie funkcji typu callback uruchamianych w momencie interacji myszy z konkretnym oknem """
        persp = self.cam.sampleCameraFrame()
        self.imshow("Preview", persp)
        cv.setMouseCallback("Preview", self.cal.manualCalibrationMouseCallback, param=self.ip) 

    def showIdlePreview(self):
        """ Prezentacja poglądu z kamery przez uruchomieniem właściwego działania programu """
        preview = self.cam.sampleCameraFrame()
        Painter.drawWaitingToStart("Preview", preview)
 
    def run(self):
        """ Główna pętla programu kontrolująca stan oraz przebieg rozgrywki """
        while self.gui.x:
            self.handleListener()
            self.gui.update()
            self.cam.setupLit(self)
            if self.hasIdleState():
                self.showIdlePreview()
                continue
            self.cal.calibrate(self)
            persp = self.cam.sampleCameraFrame()
            self.validateLit(persp)
            self.validateCalibration(persp)
            if not self.cal.isReadyToInitGameplay():
                continue
            self.img = self.ip.getPerspectiveTransform(persp)
            img = self.img
            self.img = self.ip.setChessboardDirection(self.img)
            chessboard_corner_points = self.cal.getChessboardCornerPoints(self.ip)
            Painter.drawPoints(chessboard_corner_points, persp)
            Painter.drawCalibrated("Preview", persp)
            if self.chess.gameplayHasInitialState():
                fields = self.ip.splitChessboardImageIntoFields(self.ip.picked, self.img)
                self.chess.setInitPosition(fields)
            elif self.chess.moveHasBeenTaken():
                fields = self.ip.splitChessboardImageIntoFields(self.ip.picked, self.img)
                self.chess.makeMove(fields)
                self.gui.updateGUIMoveList(self)
                print("++-- -------------------- --++")
            elif self.chess.moveIsReverted():
                fields = self.ip.splitChessboardImageIntoFields(self.ip.picked, self.img)
                self.chess.revertMove(fields)
                self.gui.updateGUIMoveList(self)
            self.chess.drawCurrentPosition(self.img)
            Painter.drawCurrentPlayerOnPreviewWindow(persp, self.chess.getCurrentPlayer())
            cv.imshow("Preview", persp)
            Painter.drawWhitePlayerAtTheBottom("Gameplay", self.img)
