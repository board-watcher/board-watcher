import cv2 as cv
# Custom exceptions

class AutoCalibrationException(Exception): pass
class ExitAppException(Exception): pass
class TooManyLines(Exception): pass

def getKeyCode():
    return cv.waitKey(10) & 0xFF
    
def keyboardListener(ip, chess, driver, cal):
    key = getKeyCode()
    if key == 27: # Esc key
        raise ExitAppException
    elif key == 32:
        chess.setGameplayStateToMadeMove()
    elif key == 13: #
        ip.chessboard_direction = (ip.chessboard_direction + 1) % 2
        print("Flipped")
    elif key == 113: # q
        print("Reset")
        driver.resetGameplay()
    elif key == 8: # backspace
        print("Decalibrate")
        cal.decalibrate()
    elif key == 114: # r
        chess.setGameplayStateToRevert()
    elif key == 107: # k
        driver.idle = False
    elif key == 100: #d
        print("d")
        driver.debug = not driver.debug