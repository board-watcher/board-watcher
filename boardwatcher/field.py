import numpy as np

class Field:
    """
    Klasa reprezentująca pole szachownicy oraz akcje z nim związane \n
    field[IMG] - informacja o obrazie \n
    field[FIELD_NAME] - nazwa pola, np E4 \n
    field[COORDS] - współrzędne narożników pola \n
    field[IS_TAKEN] - informacja czy pole jest zajęte przez bierkę czy nie \n
    field[PIECE_NAME] - Nazwa bierki - zobacz piece.py \n
    field[FIELD_OWNER] - informacja, który gracz kontroluje dane pole \n
    """
    #field
    IMG = 0
    FIELD_NAME = 1
    COORDS = 2
    IS_TAKEN = 3
    PIECE_NAME = 4
    FIELD_OWNER = 5

    WHITE = "WHITE"
    BLACK = "BLACK"
    NEUTRAL = "NEUTRAL"

    # coords
    X = 0
    Y = 1

    # corners

    LEFT_TOP = 0
    RIGHT_TOP = 1
    LEFT_BOTTOM = 2
    RIGHT_BOTTOM = 3

    def getFieldCoordsByName(self, name):
        """ Pobranie współrzędnych narożników pola o podanej nazwie np 'E4' """
        if self.fields is None:
            return None
        for x in self.fields:
            if(x[1] == name):
                return x[2]
        return None
    
    def isTaken(self, field):
        """ Informacja, czy pole jest zajęte przez bierkę """
        return True if field[3] == True else False

    def fieldHeldByWhite(self, field):
        """ Informacja, czy pole jest zajęte przez gracza białego """
        return True if field[self.FIELD_OWNER] == self.WHITE else False

    def fieldHeldByBlack(self, field):
        """ Informacja, czy pole jest zajęte przez gracza czarnego """
        return True if field[self.FIELD_OWNER] == self.BLACK else False

    def _fieldIsTaken(self, field):
        """ Informacja, czy pole jest zajęte przez jakiegokolwiek gracza"""
        return True if field[self.IS_TAKEN] else False

    def getRow(self, index):
        """ Pobranie rzędu pól wg indexu """
        return self.fields[index * 8 : index * 8 + 8]

    def _getFieldNameByIndex(self, index):
        """ Pobranie nazwy pola wg indeksu """
        return self.fields[index][self.FIELD_NAME]

    def _getFieldByIndex(self, index):
        """ Pobranie obiektu pola wg indexu """
        return self.fields[index]

    def getFieldOwnerByIndexInInitPosition(self, index):
        """ Informacja, do kogo należy pole w pozycji początkowej bierek """
        if index < 16:
            return self.WHITE
        elif index > 47:
            return self.BLACK
        else:
            return self.NEUTRAL

    def isStartupField(self, index):
        """ Informacja czy wskazane pole jest polem startowym dla bierki """
        return True if index < 16 or index > 47 else False

    def _fieldChanged(self, x, y, i, threshold = 2.137):
        """
        WAŻNE
        Metoda otrzymuje jako argumenty 2 pola o tej samej nazwie
        Jedno pole jest z informacją z poprzedniego zatwierdzonego ruchu natomiast
        drugie jest z aktualnie zatwierdzonego ruchu
        Liczona jest średnia z pikseli dla obu obrazów a następnie liczona jest różnica
        Jeżeli różnica średniej wartości pikseli jest wyższa(niższa) od podanej jako argument wywołania
        wartości progowania zwracana jest stosowna informacja
        """
        field_mean = self._getMeanOfTwoFields(x, y)
        name = self._getFieldNameByIndex(i)
        if field_mean > threshold:
            print("Mean of", name, ": ", field_mean)
        return True if field_mean > threshold else False

    def _getMeanOfTwoFields(self, x, y):
        """ Metoda używana przez metodę _fieldChanged do obliczania różnicy średniej wartości dwóch pól """
        length = len(x[self.IMG])
        ratio = length * 0.2 # less means more field area is considered
        minim = int(ratio)
        maxim = int(length - ratio)
        mean = np.absolute(x[self.IMG][minim:maxim, minim:maxim].mean() - y[self.IMG][minim:maxim, minim:maxim].mean())
        #mean = np.absolute(x[self.IMG].mean() - y[self.IMG].mean())

        return mean


    def getFieldByCoords(self, x, y):
        """ Pobranie pola poprzez kliknięcie w konkrente miejsce myszką na oknie 'Gameplay', używane do debugowania """
        for a in self.fields:
            if a[self.COORDS][self.LEFT_TOP][self.X] < x and a[self.COORDS][self.RIGHT_TOP][self.X] > x and a[self.COORDS][self.LEFT_TOP][self.Y] < y and a[self.COORDS][self.LEFT_BOTTOM][self.Y] > y:
                return a
        return None
    
    def getRowColByField(self, field):
        """ Pobranie pary indeksów rzędu oraz kolumny dla podanego pola """
        index = self.getIndexByField(field)
        row = int(index / 8)
        col = index % 8

        return (row, col)
        
    def getIndexByField(self, field):
        """ Pobranie indeksu dla pola podanego jako argument """
        for i, x in enumerate(self.fields):
            if x[self.FIELD_NAME] == field[self.FIELD_NAME]:
                return i
        return None