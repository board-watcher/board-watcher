from boardwatcher import ML
from tkinter import messagebox
from boardwatcher.calibration import Calibration
import tkinter as tk
from tkinter.constants import BOTH, BOTTOM, END, LEFT, RIGHT, TOP
from tkinter import Menu, ttk
from ttkthemes import ThemedStyle
from functools import partial
from tkinter import filedialog
from PIL import Image, ImageTk
import cv2 as cv
from pathlib import Path, PurePath
from tkinter.messagebox import showinfo
 
class GUI:
    def __init__(self, game_id, cal, ip, chess, driver):
 
        self.cal = cal
        self.ip = ip
        self.id = chess.getGameId()
        self.driver = driver
        self.gui = tk.Tk()
        self.style = ThemedStyle(self.gui)
        self.style.set_theme("arc")
        self.gui.resizable(0,0)
        self.gui.geometry("500x540")
        self.gui.title("Board Watcher Core")
        self.gui.bind("r", lambda x: chess.setGameplayStateToRevert())
        self.gui.bind("<space>", lambda x: chess.setGameplayStateToMadeMove())
        self.gui.bind("<BackSpace>", lambda x: driver.resetGameplay())
        self.gui.bind("<Escape>", lambda x: self.onClosing())
 
        self.id = game_id
        self.q = False
        self.x = True
 
        menubar = Menu(self.gui)
        self.gui.config(menu=menubar)
        self.gui.protocol("WM_DELETE_WINDOW", self.onClosing)
        subMenu = Menu(menubar, tearoff=0)
 
        self.first_frame = ttk.Frame(self.gui,width = 250,height=440)
        self.text_frame = ttk.Frame(self.first_frame)
        self.pgn_move_list = tk.Text(self.text_frame,width=28,height=380)
        self.pgn_move_list.configure(state="disabled")
        self.pgn_move_list.pack()
        self.text_frame.pack(side=TOP, padx=10, pady=10)
        self.first_frame.pack(side=LEFT)
 
        self.second_frame = ttk.Frame(self.gui,width = 250,height=440)
        self.menu_frame = ttk.Frame(self.second_frame)
        self.id_frame = ttk.Frame(self.second_frame, width=250, height=50)
        self.id_label = ttk.Label(self.id_frame, text="Game id: ", font=('bold', 12))
        self.id_label.pack(side=LEFT)
        self.number_id_label = ttk.Label(self.id_frame, text=self.id, font=('bold', 11))
        self.number_id_label.pack(side=LEFT)
        self.id_frame.pack(side=TOP,pady=10)
        self.copy_button = ttk.Button(self.second_frame, text="Copy game id", width=15, command=self.copyButton)
        self.copy_button.pack(side=TOP)
        self.setting_frame = ttk.Frame(self.second_frame)
        self.setting_label = ttk.Label(self.setting_frame, text="Calibration mode:")
        self.setting_label.pack(side=TOP)
 
        # Auto <-> Manual
        self.selected_option = tk.StringVar()
        self.selected_option.set('A')
        self.selectMode()
        self.options = (('Auto','A'),('Manual','M'))
        self.selected_option.set("A")
        for x in self.options:
            self.opt = ttk.Radiobutton(self.setting_frame, text=x[0], value=x[1], variable=self.selected_option, command=self.selectMode)
            self.opt.pack(side=LEFT, padx=5)
 
 
        self.start_button = ttk.Button(self.second_frame, text='START', command=self.breakIdle)
        self.start_button.pack(side=TOP, pady=10)
        self.setting_frame.pack(side=TOP, pady=10)
        self.threshold_label = ttk.Label(self.second_frame, text="Gameplay threshold")
        self.threshold_label.pack(side=TOP)
        self.threshold_frame = ttk.Frame(self.second_frame)
 
        self.threshold_var = tk.StringVar()
        # self.threshold_var.set('300')
        self.threshold_slider = ttk.Scale(self.threshold_frame, from_=0, to=300, value=300, orient=tk.HORIZONTAL,length=150, command=self.setThreshold)
        self.threshold_slider.pack(side=LEFT, padx=5)
 
 
        self.threshold_result = ttk.Label(self.threshold_frame, textvariable=self.threshold_var)
        self.threshold_result.pack(side=LEFT, padx=3)
        self.threshold_frame.pack(side=TOP, pady=5)
        self.perspective_threshold_label = ttk.Label(self.second_frame, text="Preview threshhold")
        self.perspective_threshold_label.pack(side=TOP)
        self.perspective_threshold_frame = ttk.Frame(self.second_frame)
 
 
        self.persp_threshold_var = tk.StringVar()
        # self.persp_threshold_var.set('300')
 
 
        self.perspective_threshold_slider = ttk.Scale(self.perspective_threshold_frame, value=300, from_=0, to=300, orient=tk.HORIZONTAL,length=150, command=self.setPerspThreshold)
        self.perspective_threshold_slider.pack(side=LEFT)
        self.perspective_threshold_result = ttk.Label(self.perspective_threshold_frame, textvariable=self.persp_threshold_var)
        self.perspective_threshold_result.pack(side=LEFT, padx=3)
        self.perspective_threshold_frame.pack(side=TOP,pady=5)
 
 
        self.calibrate_button = ttk.Button(self.second_frame, text='Calibrate', command=self.cal.decalibrate, state="disabled")
        self.calibrate_button.pack(side=TOP, pady=5)
        self.export_button = ttk.Button(self.second_frame, text="Export to PGN", width = 15, command=self.export)
        self.export_button.pack(side=TOP, pady=5, padx=10)
        self.ml_frame = ttk.Frame(self.menu_frame)
        self.ml_label = ttk.Label(self.ml_frame, text="AI Recognisation")
        self.ml_label.pack(side=TOP)
        self.load_button = ttk.Button(self.ml_frame, text= "Load image", width= 15, command=self.selectFile)
        self.load_button.pack(side=TOP, pady=5, padx=10)
        self.ml_frame.pack(side=BOTTOM)
        self.menu_frame.pack(side = TOP,pady=20)
 
        self.legend_frame = ttk.LabelFrame(self.second_frame, text="Control", width=500, height=300)
        # self.img = tk.PhotoImage(file="spacebar.png")
        self.img2 = tk.PhotoImage(file="r.png")
        self.img3 = tk.PhotoImage(file="backspace.png")
        self.img4 = tk.PhotoImage(file="k.png")
        self.calibrate_frame = tk.Frame(self.legend_frame)
        self.label = ttk.Label(self.calibrate_frame, text="Space")
        self.label.pack(side=LEFT,padx=5)
        self.move_button = ttk.Button(self.calibrate_frame, text="Move - Confirm", command=self.driver.chess.setGameplayStateToMadeMove)
        self.move_button.pack(side=BOTTOM)
        self.move_button = ttk.Button(self.calibrate_frame, text="Move - Revert", command=self.driver.chess.setGameplayStateToRevert)
        self.move_button.pack(side=BOTTOM)
        self.move_button = ttk.Button(self.calibrate_frame, text="Move - Revert", command=self.driver.chess.setGameplayStateToRevert)
        self.move_button.pack(side=BOTTOM)
        self.label3 = ttk.Label(self.calibrate_frame, image=self.img2)
        self.label3.pack(side=LEFT, padx=5)
        self.label4 = ttk.Label(self.calibrate_frame, text="Previous")
        self.label4.pack(side=LEFT)
        self.calibrate_frame.pack(side=TOP, padx=5, pady=10)
        self.calibrate_frame2 = tk.Frame(self.legend_frame)
        self.label5 = ttk.Label(self.calibrate_frame2, image=self.img3)
        self.label5.pack(side=LEFT, padx=5)
        self.label6 = ttk.Label(self.calibrate_frame2, text="Reset")
        self.label6.pack(side=LEFT)
        self.label7 = ttk.Label(self.calibrate_frame2, image=self.img4)
        self.label7.pack(side=LEFT, padx=7)
        self.label8 = ttk.Label(self.calibrate_frame2, text="Start")
        self.label8.pack(side=LEFT)
        self.calibrate_frame2.pack(side=LEFT, padx=5, pady=10)
        self.legend_frame.pack(side=BOTTOM, padx=10)
        self.second_frame.pack(side=LEFT, pady=15, padx=8)

    def instruction(self):
        """ Informacja o twórcach """
        ttk.messagebox.showinfo(title='About Board Watcher', message='Board Watcher - Zespół nr 2')

    def setThreshold(self, x):
        """ Ustawienie progowania dla rzutu 2D """
        self.cal.threshold = int(float(x))

    def setPerspThreshold(self, x):
        """ Ustawienie progowania dla próbki z kamery """
        self.cal.persp_threshold = int(float(x))

    def breakIdle(self):
        """ Przerwanie trybu bezczynności """
        self.driver.activate()
        self.start_button.configure(state="disabled")
        self.load_button.configure(state="disabled")
        self.calibrate_button.configure(state="normal")

    def onClosing(self):
        """ Zamykanie aplikacji """
        if messagebox.askquestion("Exit", "Do you want to quit the application?") == 'yes':
            self.x = False
            self.gui.destroy()
 
    def copyButton(self):
        """ Skopiowanie do schowka ID gry """
        self.gui.clipboard_clear()
        self.gui.clipboard_append(self.id)
 
 
    def selectMode(self):
        """ Wybór pomiędzy automaczycznym i manulanym trybem kalibrowania """
        self.ip.removeChessboardCornerPoints()
        self.ip.removePicked()
        self.select = self.selected_option.get()
        if self.select == "M":
            self.cal.auto_calibration = False
            self.cal.calibrated = False
        else:
            self.cal.auto_calibration = True
            self.cal.calibrated = False
 
    def selectFile(self):
        """ Okno dialogowe z wyborem pliku """
        filetypes = (
            ('All files', '*.*'),
            ('Png', '*.png'),
            ('Jpg', '*.jpg')
        )
        filename = filedialog.askopenfilename(initialdir = "/", filetypes=filetypes)
        if filename!="":
            tk.messagebox.showinfo(title='Model load', message=' Click OK to load the model. Process may take up to a minute.')
            ML.run(filename, self.id)
        else:
            tk.messagebox.showerror(title='Error load image', message='No selected the file!')

 
    def export(self):
        """ Zapis bieżącego stanu partii do pliku PGN """
        filename = filedialog.asksaveasfilename(filetypes=[("Portable Game Notation","*.pgn")], defaultextension = "*.pgn", initialdir = "/")
        if filename:
            with open(filename, "w", -1, "utf-8") as file:
                file.write(self.pgn_move_list.get(1.0, END))
 
    def update(self):
        """ Metoda renderująca bieżący stan GUI """
        if self.x == True:
            self.gui.update_idletasks()
            self.gui.update()
 
    def updateMoves(self, moves):
        """ Oświeżanie listy ruchów w GUI """
        moves_count = len(moves)
        self.pgn_move_list.configure(state="normal")
        self.pgn_move_list.delete(1.0, END)
        for i, x in enumerate(moves):
            item = str(int(((i + 1) / 2) + 0.5)) + ". " +  x if i % 2 == 0 else " " + x + "\n"
            self.pgn_move_list.insert(END, item)
 
        self.pgn_move_list.configure(state="disabled")
 
 
    def resetPGN(self):
        """ Zresetowanie listy ruchów w GUI """
        self.pgn_move_list.configure(state="normal")
        self.pgn_move_list.delete(1.0, END)
        self.pgn_move_list.configure(state="disabled")
 
    def updateGUIMoveList(self, driver):
        """ Pobranie listy ruchów z obiektu rozgrywki i wywołanie metody aktualizującej stan ruchów """
        moves = driver.chess.getPGNMoves()
        self.updateMoves(moves)
 
    def setTrackbarPos(self, name, val):
        """ Ustawienie wartości suwaka """
        cv.setTrackbarPos(name, "Settings" , val)
 
def initGui(driver):
    """ Inicjalizacja GUI z biblioteki OpenCV """
    cv.namedWindow("Settings", cv.WINDOW_GUI_NORMAL)
    if driver.debug:
        cv.createTrackbar('Lines', "Settings", driver.line_mode, 5, partial(showLines, driver= driver))
        cv.createTrackbar('Intersections', "Settings", driver.intersection_vis, 1, partial(showIntersections, driver= driver))
    cv.createTrackbar('Exposure', "Settings", 0, 20, partial(setExposure, driver= driver))
 
def setTrackbarPos(name, val):
    cv.setTrackbarPos(name, "Settings" , val)
 
def setExposure(x, driver):
    driver.cam.set(cv.CAP_PROP_EXPOSURE, float(-x/2))
    driver.exposure = float(-x / 2)
 
def showLines(x, driver):
    driver.line_mode = x
 
def showIntersections(x, driver):
    driver.intersection_vis = not driver.intersection_vis