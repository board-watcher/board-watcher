import cv2 as cv
import numpy as np
from boardwatcher import lines as l, gui
from boardwatcher.exception import AutoCalibrationException

class ImageProcessor: 
    """
    Klasa zarządzająca przetwarzaniem obrazu szachownicy
    """

    CORRECT_CHESSBOARD = 81
    chessboard_direction = 0

    def __init__(self):
        """ Konstruktor, zdefiniowanie wewnętrznych pól klasy """
        # self.rectangle = None !! gdyby się wywaliło na głupi ryj to tutaj !
        self.rectangle = []
        self.picked = []

    def notEnoughtIntersections(self):
        """ Informacja, czy liczba przecięć jest mniejsza niż 81 """
        return True if len(self.picked) < self.CORRECT_CHESSBOARD else False

    def hasPickedPoints(self):
        """ Informacja, czy liczba przecięć jest równa 81 """
        return True if len(self.picked) == self.CORRECT_CHESSBOARD else False
        
    def hasChessboardCorners(self):
        """ Informacja, czy liczba wyznaczonych narożników jest równa 4 """
        return True if len(self.rectangle) == 4 else False

    def removeChessboardCornerPoints(self):
        """ Usunięcie zapisanych narożników szachownicy """
        self.rectangle = []
    
    def removePicked(self):
        """ Usunięcie listy przecięć pól """
        self.picked = []

    def setChessboardCornersByLinesLimitedByCameraResolution(self, lines, resolution):
        """ Linie podane jako argument wywołania są przetwarzane w celu uzyskania skrajnych punktów narożnych szachownicy """
        if lines is None:  # jezeli nie ma pustych linii to
            raise AutoCalibrationException("Chessboard not found")

        segmented = l.segment_by_angle_kmeans(lines)
        intersections = l.segmented_intersections(segmented)
        if intersections is None or len(intersections) < 2:
            raise AutoCalibrationException("Chessboard not found")
        max_sum = 0
        min_sum = 10000
        max_diff = 0
        min_diff = 10000
        botleft = None
        botright = None
        topleft = None
        topright = None
        
        for point in intersections:
            if self.__pointOutOfView(point, resolution):
                # xd = np.zeros(resolution, np.uint8)
                # cv.putText(xd, str((point[0], point[1])),(point[0], point[1]), cv.FONT_HERSHEY_SIMPLEX, 0.5,(255, 255, 255),2,cv.LINE_AA)
                # cv.imshow("XY", xd)
                continue
            if max_sum < point[0] + point[1]:
                max_sum = point[0] + point[1]
                botright = tuple((point[0], point[1]))
            if min_sum > point[0] + point[1]:
                min_sum = point[0] + point[1]
                topleft = tuple((point[0], point[1]))
            if max_diff < abs(point[0] - point[1]):
                max_diff = point[0] - point[1]
                topright = tuple((point[0], point[1]))
            if min_diff > point[0] - point[1]:
                min_diff = point[0] - point[1]
                botleft = tuple((point[0], point[1]))

        if botleft == None or botright == None or topleft == None or topright == None:
            raise AutoCalibrationException("Chessboard not found")

        self.rectangle = (botleft, topleft, topright, botright)

    def __pointOutOfView(self, point, res):
        """ Informacja, czy współrzędne punktu są pola obszarem widocznym """
        res_x = res[1] # to je poodwracane jak coś
        res_y = res[0]

        return True if point[0] > res_x or point[0] < 0 or point[1] > res_y or point[1] < 0 else False
    
    def sortIntersections(self):
        """ Sortowanie listy znalezionych przecięć """
        sorted_intersections = []
        sortedY = sorted(self.picked, key= lambda k : k[1])

        for i in range (0, 82, 9):
            row = sortedY[0 + i : 9 + i]
            sorted_intersections.extend(sorted(row, key= lambda k : k[0]))

        self.picked = sorted_intersections
    
    def getPerspectiveTransform(self, img):
        """ Zwrócenie rzutu 2D z podanego jako argument wywołania obrazu na podstawie 4 punktów narożnych szachownicy """
        drawed_img = img.copy()
        min2 = self.rectangle[0]
        min3 = self.rectangle[1]
        min22 = self.rectangle[2]
        min33 = self.rectangle[3]

        pts3 = np.float32([[min2[0], min2[1]], [min33[0], min33[1]], [
            min22[0], min22[1]], [min3[0], min3[1]]])
        pts4 = np.float32([[0, 0], [800, 0], [800, 800], [0, 800]])
        M4 = cv.getPerspectiveTransform(pts3, pts4)
        dst4 = cv.warpPerspective(drawed_img, M4, (800, 800))
        dst4 = dst4[20 : 780, 20: 780]
        res = cv.flip(dst4, 1)

        return res

    def getImgCornerRects(self, img, cam):
        """ Pobranie wartości rastrowej prostokątów o małym rozmiarze z czterech punktów szachownicy """
        p0 = img[max(0, self.rectangle[0][1] - 10) : min(cam.res_y, self.rectangle[0][1] + 10), max(0, self.rectangle[0][0] - 10) : min(cam.res_x, self.rectangle[0][0] + 10)].copy()
        p1 = img[max(0, self.rectangle[1][1] - 10) : min(cam.res_y, self.rectangle[1][1] + 10), max(0, self.rectangle[1][0] - 10) : min(cam.res_x, self.rectangle[1][0] + 10)].copy()
        p2 = img[max(0, self.rectangle[2][1] - 10) : min(cam.res_y, self.rectangle[2][1] + 10), max(0, self.rectangle[2][0] - 10) : min(cam.res_x, self.rectangle[2][0] + 10)].copy()
        p3 = img[max(0, self.rectangle[3][1] - 10) : min(cam.res_y, self.rectangle[3][1] + 10), max(0, self.rectangle[3][0] - 10) : min(cam.res_x, self.rectangle[3][0] + 10)].copy()
        
        return (p0, p1, p2, p3)

    def chessboardMoved(self, persp, cam):
        """ Informacja czy szachownica została przesunięta, następuje tutaj porównanie wycinków narożników z ruchu bieżącego i poprzedniego """
        current = self.getImgCornerRects(persp, cam)
        # cv.imshow("Curr",current[3])
        # cv.imshow("Origin",self.corners[3])
        # cv.imshow("Mixed", cv.absdiff(self.corners[3], current[3]))
        if np.absolute(current[3].mean() - self.corners[3].mean()) > 6 and np.absolute(current[1].mean() - self.corners[1].mean()) > 6 and np.absolute(current[2].mean() - self.corners[2].mean()) > 6 and np.absolute(current[3].mean() - self.corners[3].mean()) > 6:
            return True
        else:
            return False

    def setChessboardDirection(self, img):
        """ Ustawienie kierunku szachownicy w oknie 'Gameplay' """
        if self.chessboard_direction == 0:
            img = cv.rotate(img, cv.ROTATE_90_COUNTERCLOCKWISE)
        elif self.chessboard_direction == 1:
            img = cv.rotate(img, cv.ROTATE_90_CLOCKWISE)

        return img
        
    def getChessboardContours(self, img):
        gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
        mask = np.zeros((gray.shape),np.uint8)
        thresh = cv.adaptiveThreshold(gray,255,0,1,19,2) 
        contours, hierarchy = cv.findContours(thresh, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)

        img_countours = np.zeros(img.shape)
        maks = 0
        indeks = 0
        best_cnt = None
        for i in range(0, len(contours)):
            cnt = contours[i]
            area = cv.contourArea(cnt) 
            if area > maks:
                maks = area
                best_cnt = cnt  
    
        cv.drawContours(mask,[best_cnt],0,255,-1)
        cv.drawContours(mask,[best_cnt],0,0,2)
        res = cv.bitwise_and(img, img, mask=mask)

        return res


    def pick_points(self, points):
        c = points - [400, 400]
        center_ind = np.argwhere(np.sqrt(np.sum(c*c, axis=1))
                                == np.min(np.sqrt(np.sum(c*c, axis=1))))
        center = points[center_ind][0][0]

        top_x = []
        top_y = []
        bottom_x = []
        bottom_y = []

        for point in points:
            if np.all(point == center):
                continue
            if point[0] > center[0] and (point[1] < center[1] + 15 and point[1] > center[1] - 15):
                top_x.append(point)
            if point[0] < center[0] and (point[1] < center[1] + 15 and point[1] > center[1] - 15):
                bottom_x.append(point)
            if point[1] > center[1] and (point[0] < center[0] + 15 and point[0] > center[0] - 15):
                top_y.append(point)
            if point[1] < center[1] and (point[0] < center[0] + 15 and point[0] > center[0] - 15):
                bottom_y.append(point)

        top_x = sorted(top_x, key=lambda k: k[0])[:4]
        bottom_x = sorted(bottom_x, key=lambda k: k[0], reverse=True)[:4]
        top_y = sorted(top_y, key=lambda k: k[1])[:4]
        bottom_y = sorted(bottom_y, key=lambda k: k[1], reverse=True)[:4]

        picked_points = []
        for x, _ in top_x + bottom_x + [center]:
            for _, y in top_y + bottom_y + [center]:
                picked_points.append([x, y])

        return picked_points

    def splitChessboardImageIntoFields(self, intersections, img):
        fields = []
        img = cv.GaussianBlur(img, (5, 5), 0)
        matrix = np.reshape(intersections, (9, 9, 2))
        for i in range(0, 8):
            for j in range(0, 8):
                source_coords = np.float32(
                    [matrix[i][j], matrix[i][j + 1], matrix[i + 1][j], matrix[i + 1][j + 1]])
                dest_coords = np.float32([[0, 0], [78, 0], [0, 78], [78, 78]])
                transform_matrix = cv.getPerspectiveTransform(
                    source_coords, dest_coords)

                fields.append([cv.warpPerspective(img, transform_matrix, (78, 78)), chr(
                    72 - j) + str(i+1), source_coords])

        return fields
