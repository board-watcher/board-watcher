import cv2 as cv 
import numpy as np
import math
import boardwatcher
import boardwatcher.exception

from collections import defaultdict

def getIntersectionsByLines(lines):
    """ """
    segmented = segment_by_angle_kmeans(lines)
    intersections = segmented_intersections(segmented)
    intersections = filterIntersections(intersections)

    return intersections


def intersection(line1, line2):

    rho1, theta1 = line1[0]
    rho2, theta2 = line2[0]
    C = np.array([
        [np.cos(theta1), np.sin(theta1)],
        [np.cos(theta2), np.sin(theta2)]
    ])
    r = np.array([[rho1], [rho2]])
    wyznacznik = (C[0][0] * C[1][1]) - (C[0][1] * C[1][0])

    if wyznacznik != 0:
        x1, y1 = np.linalg.solve(C, r)
        x1, y1 = int(np.round(x1)), int(np.round(y1))
        return [x1, y1]



# list_of_intersections has to be sorted
def find_two_outer_corners(list_of_intersections, img):
    length = len(list_of_intersections)
    minvalue = list_of_intersections[0]
    maxvalue = list_of_intersections[length-1]
    cv.circle(img, (minvalue[0], minvalue[1]),
               radius=6, color=(0, 0, 255), thickness=-1)  # min,min
    cv.circle(img, (maxvalue[0], maxvalue[1]),
               radius=6, color=(0, 0, 255), thickness=-1)  # max,max

    return minvalue, maxvalue

# list_of_intersections has to be sorted
# no draw
def findTwoOuterPoints(list_of_intersections):
    length = len(list_of_intersections)
    minvalue = list_of_intersections[0]
    maxvalue = list_of_intersections[length-1]

    return minvalue, maxvalue

def segment_by_angle_kmeans(lines, k=2, **kwargs):
    """Groups lines based on angle with k-means.

    Uses k-means on the coordinates of the angle on the unit circle 
    to segment `k` angles inside `lines`.
    """

    # Define criteria = (type, max_iter, epsilon)
    default_criteria_type = cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER
    criteria = kwargs.get('criteria', (default_criteria_type, 10, 1.0))
    flags = kwargs.get('flags', cv.KMEANS_RANDOM_CENTERS)
    attempts = kwargs.get('attempts', 10)

    # returns angles in [0, pi] in radians
    angles = np.array([line[0][1] for line in lines])
    # multiply the angles by two and find coordinates of that angle
    pts = np.array([[np.cos(2*angle), np.sin(2*angle)]
                    for angle in angles], dtype=np.float32)

    # run kmeans on the coords
    #num_rows = np.shape(pts)[0]
    #if num_rows > k: /ten if prawda to zrób kmeans, w przeciwnym wypadku wywal jakiś wyjątek TODO
    labels, centers = cv.kmeans(pts, k, None, criteria, attempts, flags)[1:]
    labels = labels.reshape(-1)  # transpose to row vec

    # segment lines based on their kmeans label
    segmented = defaultdict(list)
    for i, line in zip(range(len(lines)), lines):
        segmented[labels[i]].append(line)
    segmented = list(segmented.values())
    return segmented



def segmented_intersections(lines):
    """Finds the intersections between groups of lines."""

    intersections = []
    for i, group in enumerate(lines[:-1]):
        for next_group in lines[i+1:]:
            for line1 in group:
                for line2 in next_group:
                    if(intersection(line1, line2) != None):
                        intersections.append(intersection(line1, line2)) 

    return intersections


def getLines(img, threshold):
    gray = cv.cvtColor(img,cv.COLOR_BGR2GRAY)
    edges = cv.Canny(gray,50,150,apertureSize = 3)

    # gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    # gray = cv.blur(gray, (3,3))
    # v = np.median(gray)
    # lower = int(min(0, (1.0-0.33) * v ))
    # upper = int(max(255, (1.0+0.33) * v ))
    # edges = cv.Canny(gray, lower, upper)

    lines = cv.HoughLines(edges, 1, np.pi/180, threshold)

    if lines is None:
        lines = []

    return lines

def getColor(i, current_line):
    if(current_line == i):
        color = (0, 255, 0)
    else:
        color = (0, 0, 255)
        # color = colors[i]
    
    return color

def drawLines(lines, img, driver, color = None, thickness = 2):
    if(driver.line_mode):
        for i, line in enumerate(lines):
            rho,theta = line[0]
            a = np.cos(theta)
            b = np.sin(theta)
            x0 = a*rho
            y0 = b*rho
            x1 = int(x0 + 1000*(-b))
            y1 = int(y0 + 1000*(a))
            x2 = int(x0 - 1000*(-b))
            y2 = int(y0 - 1000*(a))
            if(color is not None):
                cv.line(img,(x1,y1),(x2,y2), color, thickness)
            else:
                cv.line(img,(x1,y1),(x2,y2), getColor(i, driver.current_line), thickness)

def filterLinesV2(lines, theta = 2):
    arr = []
    aux = []
    for i, x in enumerate(lines):
        arr.append(x)
        
    for i, x in enumerate(arr):
        del_count = 0
        for j, y in enumerate(arr[i+1:]):
            if np.logical_and(abs(x[0][0] - y[0][0]) < 5.5, abs(x[0][1] - y[0][1]) < 0.01):
                try:
                    del arr[i + 1 + j - del_count]
                    del_count = del_count + 1
                except Exception:
                    print("Attempt to delete cos co not exists")
    return np.array(arr)

def filterIntersections(intersections):
    aux = []
    res = []
    org = intersections
        
    for i, point in enumerate(intersections):
        del_count = 0
        aux.append(point)
        for j, iterated_point in enumerate(intersections[i+1:]):
            if(abs(point[0] - iterated_point[0]) < 20 and abs(point[1] - iterated_point[1]) < 20):
                aux.append(iterated_point)
                #intersections.remove(iterated_point)
                del intersections[i + 1 + j - del_count]
                del_count = del_count + 1
                
        x, y = np.mean(aux, axis=0)
        res.append((int(x), int(y)))
        aux = []

    return np.array(res)

def fillGapsWithAlmostPerpendicularLines(perpendicular, almost_perpendicular, driver):
    result = perpendicular
    for x in almost_perpendicular:
        toAppend = True
        for y in perpendicular:
            if (abs(x[0][0] - y[0][0]) < driver.generic):
                toAppend = False
                break
        if toAppend:
            result.append(x) # Jeżeli dana linia dodatkowa nie była blisko linii bazowej to znaczy, że wnosi do obrazu nowe istotne informacje
    return result

def filterLinesV3(lines):
    lines = np.sort(lines, axis=0)
    arr = []
    arr.append(lines[0])
    for i in range(1, len(lines)):
        if (lines[i][0][0] - lines[i-1][0][0]) > 3:
            arr.append(lines[i])
        
    return arr

def getLinesInSimilarDistanceBetween(lines):
    pass


def extractPerpendicular(lines, driver):
    # Nie polecam 'vertical = horizontal = almost_vertival = almost_horizontal = []' szukałem godzinę czemu nie działa
    vertical = []
    horizontal = []
    almost_vertival = []
    almost_horizontal = []
    for line in lines:
        if(line[0][1] < 0.01): # 1.5707964 == 90degree
            vertical.append(line)
        elif (abs(line[0][1] - 1.5707964) < 0.01 ):
            horizontal.append(line)
        elif (line[0][1] >= 0.01 and line[0][1] < 0.05):
            line[0][1] = 0
            almost_vertival.append(line)
        elif (abs(line[0][1] - 1.5707964) >= 0.01 and abs(line[0][1] - 1.5707964) < 0.05):
            line[0][1] = 1.5707964
            almost_horizontal.append(line)

    # vertical = filterLinesV3(horizontal)

    res_vertical = vertical
    # res_vertical = fillGapsWithAlmostPerpendicularLines(vertical, almost_vertival, driver)
    # res_horizontal = fillGapsWithAlmostPerpendicularLines(horizontal, almost_horizontal, driver)
    res_horizontal = horizontal

    # if driver.calibrated:
    #     res_vertical = filterLinesV3(res_vertical)
    #     res_horizontal = filterLinesV3(res_horizontal)

    return np.array(res_vertical + res_horizontal), np.array(vertical + horizontal), np.array(almost_vertival + almost_horizontal)