import cv2 as cv
import numpy as np
from boardwatcher import lines as l
class Painter():
    """
    Klasa z metodami do rysowania na komponentach
    """

    @staticmethod
    def drawCalibrated(winname, img):
        """ Rysowanie informacji o tym, że kalibrowanie zostało zakończone pomyślnie """
        cv.putText(img, "CALIBRATED",(30, 25), cv.FONT_HERSHEY_SIMPLEX, 0.5,(50, 255, 50),2,cv.LINE_AA)
        cv.imshow(winname, img)

    @staticmethod
    def drawWaitingToStart(winname, img):
        """ Rysowanie informacji o tym, że aplikacja jest w trubie bezczynności"""
        cv.putText(img, "WAITING TO START",(30, 25), cv.FONT_HERSHEY_SIMPLEX, 0.5,(251, 250, 240),2,cv.LINE_AA)
        cv.imshow(winname, img)

    @staticmethod
    def drawAutoCalibrationInProgress(winname, img):
        """ Rysowanie informacji o tym, że kalibrowanie w trybie automacznym jest w trakcie"""
        cv.putText(img, "AUTO CALIBRATION IN PROGRESS",(30, 25), cv.FONT_HERSHEY_SIMPLEX, 0.5,(255, 60, 50),2,cv.LINE_AA)
        cv.imshow(winname, img)

    @staticmethod
    def drawManualCalibrationInProgress(winname, img):
        """ Rysowanie informacji o tym, że kalibrowanie w trybie manualnym jest w trakcie"""
        cv.putText(img, "MANUAL CALIBRATION IN PROGRESS",(30, 25), cv.FONT_HERSHEY_SIMPLEX, 0.5,(255, 60, 50),2,cv.LINE_AA)
        cv.imshow(winname, img)

    def drawByCoors(self, img, field, color = (0, 255, 0)):
        """ Rysowanie prostokątu na podstawie pola podanego jako argument wywołania """
        cv.rectangle(img, (field[self.LEFT_TOP][self.X], field[self.LEFT_TOP][self.Y]), (field[self.RIGHT_BOTTOM][self.X], field[self.RIGHT_BOTTOM][self.Y]), color, 5)

    def drawLines(self, img, persp, basic, lines, perpendicular_lines, additional_lines):
        """ Funkcja do debugowania - rysowanie znalezionych linii w rzucie 2D """
        a = self.line_mode
        if a == 0:
            cv.putText(persp, "No Lines",(30, 50), cv.FONT_HERSHEY_SIMPLEX, 0.5,(255, 255, 255),2,cv.LINE_AA)
        elif a == 1:
            cv.putText(persp, "Result",(30, 50), cv.FONT_HERSHEY_SIMPLEX, 0.5,(255, 255, 255),2,cv.LINE_AA)
            l.drawLines(lines, img, self, (155, 0, 0))
        elif a == 2:
            cv.putText(persp, "Perpendicular",(30, 50), cv.FONT_HERSHEY_SIMPLEX, 0.5,(255, 255, 255),2,cv.LINE_AA)
            l.drawLines(perpendicular_lines, img, self, (255, 0, 0))

        elif a == 3:
            cv.putText(persp, "Perpendicular and Additional",(30, 50), cv.FONT_HERSHEY_SIMPLEX, 0.5,(255, 255, 255),2,cv.LINE_AA)
            l.drawLines(perpendicular_lines, img, self, (255, 0, 0))
            l.drawLines(additional_lines, img, self, (0, 0, 255))
        elif a == 4:
            cv.putText(persp, "Additional",(30, 50), cv.FONT_HERSHEY_SIMPLEX, 0.5,(255, 255, 255),2,cv.LINE_AA)
            l.drawLines(additional_lines, img, self, (0, 0, 255))
        elif a == 5:
            cv.putText(persp, "Basic",(30, 50), cv.FONT_HERSHEY_SIMPLEX, 0.5,(255, 255, 255),2,cv.LINE_AA)
            l.drawLines(basic, img, self, (155, 0, 0))

    @staticmethod
    def drawNoChessboard(winname, cam, debug = False, intersection_count = None):
        """ Rysowanie informacji o tym, że szachownica nie została znaleziona """
        noChessboard = np.zeros(cam.get2DResolution(), np.uint8)
        render_x, render_y = cam.getTextRenderPos()
        text = "NO CHESSBOARD"
        cv.putText(noChessboard, text, (70, 70), cv.FONT_HERSHEY_SIMPLEX, 0.5,(255, 255, 255),2,cv.LINE_AA)
        if debug:
            cv.putText(noChessboard, intersection_count, (render_x + 110, render_y + 100), cv.FONT_HERSHEY_SIMPLEX, 0.5,(255, 255, 255),2,cv.LINE_AA)
            cv.putText(noChessboard, "Intersections: ", (render_x, render_y + 100), cv.FONT_HERSHEY_SIMPLEX, 0.5,(255, 255, 255),2,cv.LINE_AA)
        cv.imshow("Gameplay", noChessboard)

    @staticmethod
    def drawCurrentPlayerOnPreviewWindow(persp, current_player):
        """ Rysowanie informacji o tym, który gracz jest na posunięciu """
        cv.putText(persp, current_player + "'s move",(30, 65), cv.FONT_HERSHEY_SIMPLEX, 0.5,(255, 255, 255),2,cv.LINE_AA)

    @staticmethod
    def drawPoints(points, img, r = 255, g = 255, b = 255, radius = 5):
        """ Rysowanie punków na podstawie listy podanej jako argument wywołania """
        for intersection in points:
            cv.circle(img, (intersection[0], intersection[1]), radius, (b, g, r), -1)

    @staticmethod
    def drawIndexes(points, img, r, g, b):
        """ Rysowanie indeksów na podstawie listy podanej jako argument wywołania """
        for i, intersection in enumerate(points):
            cv.putText(img, str(i),(intersection[0], intersection[1]), cv.FONT_HERSHEY_SIMPLEX, 0.5,(b, g, r),2,cv.LINE_AA)

    @staticmethod
    def drawWhitePlayerAtTheBottom(winname, img):
        """ Rysowanie rzutu 2D w oknie 'Gameplay' """
        img = cv.rotate(img, cv.ROTATE_180)
        cv.imshow(winname, img)
