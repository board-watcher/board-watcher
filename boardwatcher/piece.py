class Piece:
    # pieces
    EMPTY = "EMPTY"
    PAWN = "PAWN"
    ROOK = "ROOK"
    BISHOP = "BISHOP"
    KNIGHT = "KNIGHT"
    QUEEN = "QUEEN"
    KING = "KING"

    # sets of same pieces and corresponded fields
    ROOK_FIELDS = {0, 7, 56, 63}
    KNIGHT_FIELDS = {1, 6, 57, 62}
    BISHOP_FIELDS = {2, 5, 58, 61}
    KING_FIELDS = {3, 59}
    QUEEN_FIELDS = {4, 60}
    PAWN_FIELDS = {8, 9, 10, 11, 12, 13, 14, 15, 55, 54, 53, 52, 51, 50, 49, 48}

    WHITE_KING_STARTUP_POS = 3
    BLACK_KING_STARTUP_POS = 59

    WHITE_ROOK_KING_SIDE_STARTUP_POS = 0
    WHITE_ROOK_QUEEN_SIDE_STARTUP_POS = 7
    BLACK_ROOK_KING_SIDE_STARTUP_POS = 56
    BLACK_ROOK_QUEEN_SIDE_STARTUP_POS = 63

    def getPieceNameByIndexInInitPosition(self, i):
        if i in self.ROOK_FIELDS:
            return self.ROOK
        elif i in self.KNIGHT_FIELDS:
            return self.KNIGHT
        elif i in self.BISHOP_FIELDS:
            return self.BISHOP
        elif i in self.QUEEN_FIELDS:
            return self.QUEEN
        elif i in self.KING_FIELDS:
            return self.KING
        elif i in self.PAWN_FIELDS:
            return self.PAWN
        else:
            return self.EMPTY