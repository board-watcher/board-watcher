import cv2 as cv
import numpy as np
from boardwatcher.painter import Painter
from boardwatcher.field import Field
from boardwatcher.piece import Piece
class PieceMove(Field, Piece):
    """
    Klasa obsługująca poruszanie się bierek oraz pobieranie listy regulaminowych ruchów dla bierkek okupujących dane pole
    """

    def _getChangedFields(self, curr, previous, threshold):
        """ Pobranie listy zmienionych pól bieżącego i poprzedniego zatwierdzonego ruchu """
        changed = []
        factor = 1.5
        for i in range(len(curr)):
            if self._fieldChanged(previous[i], curr[i], i, threshold * factor):
                mean = self._getMeanOfTwoFields(previous[i], curr[i])
                changed.append((i, mean))

        return changed

    def _fieldHasAvailableMoves(self, field):
        """ Informacja, czy z pola podanego jako argument wywołania można wykonać regulaminowy ruch """
        amoves = self._getAvailableMovesByField(field)
        return True if amoves is not None and len(amoves) > 0 else False

    def _getAvailableMovesByField(self, field):
        """ Pobranie listy dostępnych ruchów z pola podanego jako argument wywołania metody """
        board = self.vectorToMatrix()
        available_moves = self.__getFieldCorrectMovesByBoard(field, board)

        return available_moves

    def __getFieldCorrectMovesByBoard(self, field, board):
        """
        Główna metoda, która w zależności jaka bierka okupuje pole podane jako argument wywołania programu
        wywołuje stosowną metodę dla konkretnej bierki
        """
        res = []
        if field[self.PIECE_NAME] == self.PAWN:
            res = self._getPawnMoves(field, board)
        elif field[self.PIECE_NAME] == self.KNIGHT:
            res = self._getKnightMoves(field, board)
        elif field[self.PIECE_NAME] == self.ROOK:
            res = self._getRookMoves(field, board)
        elif field[self.PIECE_NAME] == self.BISHOP:
            res = self._getBishopMoves(field, board)
        elif field[self.PIECE_NAME] == self.QUEEN:
            res = self._getQueenMoves(field, board)
        elif field[self.PIECE_NAME] == self.KING:
            res = self._getKingMoves(field, board)
        
        return res

    def _filterChangedFields(self, changed, current_player):
        """
        Odfiltrowanie pól, które mimo, że zmieniły się - na pewno nie mogą być rozpatrywane jako pola, które wzięły udział w zatwierdzonym ruchu
        Wynika to z lokalnych zmieniających się warunków oświetlenia stąd trzeba to wpierać programowo
        """
        current_player_fields = self.__getCurrentPlayerFields(changed, current_player)
        to_remove = self.__filterCurrentPlayerFields(current_player_fields, changed)
        changed = self.__substractSets(changed, to_remove)
        changed = self.__filterByAvailableMoves(changed, current_player)
        changed = self.__appendBoolOwnerInformationAndFullField(changed, current_player)
        changed = self.__sortStartingByCurrentPlayer(changed)

        return changed

    def __sortStartingByCurrentPlayer(self, changed):
        """
        Posortowanie kandydatów na pola, które wzięły udział w wykonanym ruchu począwszy od pól, które należą do aktualnego gracza
        oraz w następnej kolejności po współczynniku zmiany tychże pól
        """
        changed.sort(reverse = True, key=lambda x: (x[1], x[0][1]))

        return changed


    def __appendBoolOwnerInformationAndFullField(self, changed, current_player):
        """ Dołączenie dodatkowych informacji o polu """
        res = []
        for x in changed:
            field = self._getFieldByIndex(x[0])
            current_player_is_owner = True if field[self.FIELD_OWNER] == current_player else False
            res.append((x, current_player_is_owner, field))

        return res

    def __substractSets(self, changed, to_remove):
        """ Wykonanie operacji odjęcia zbiorów złożonych z pól """
        for x in to_remove:
            changed.remove(x)

        return changed

    def __getCurrentPlayerFields(self, possibly_changed, current_player):
        """ Pobranie listy pól bieżącego gracza """
        res = []
        for x in possibly_changed:
            field = self._getFieldByIndex(x[0])
            if field[self.FIELD_OWNER] == current_player:
                res.append((x, field))
        
        return res

    def __filterCurrentPlayerFields(self, current_player_fields, possibly_changed):
        """  """
        possibly_changed_names = self.__getNamesFromIndexMeanFieldTuple(possibly_changed)
        to_remove = []
        for x in current_player_fields:
            is_considered = False
            amoves = self._getAvailableMovesByField(x[1])
            for y in amoves:
                if y in possibly_changed_names:
                    is_considered = True
            if not is_considered:
                to_remove.append(x[0])
                    

        return to_remove

    def __getNamesFromIndexMeanFieldTuple(self, possibly_changed):
        res = []
        for x in possibly_changed:
            field_name = self._getFieldNameByIndex(x[0])
            res.append(field_name)

        return res

    def __filterByAvailableMoves(self, posibbly_changed, current_player):
        """
        Spośród pól podanych jako argument metody zwrócenie wyłącznie tych,
        które są puste lub pole jest w posiadaniu przeciwnika lub
        pole jest w posiadaniu bieżącego gracza a bierka okupująca dane pole ma jakikolwiek ruch
        """
        truly_changed = []
        for x in posibbly_changed:
            field = self._getFieldByIndex(x[0])
            if not self._fieldIsTaken(field) or field[self.FIELD_OWNER] != current_player or self._fieldHasAvailableMoves(field):
                truly_changed.append(x)

        return truly_changed

    def _getPawnMoves(self, field, board):
        available_moves = []
        row, col = self.getRowColByField(field)

         # black pieces move in the opposite direction
        if self.fieldHeldByWhite(field):
            forward_dir = 1 
            opponent = self.BLACK
        else:
            forward_dir = -1
            opponent = self.WHITE
        # one step forward
        if board[row + forward_dir][col][self.PIECE_NAME] == self.EMPTY:
            available_moves.append(board[row + forward_dir][col][self.FIELD_NAME])
        # capture left
        if col - 1 >= 0 and board[row + forward_dir][col - 1][self.FIELD_OWNER] == opponent:
            available_moves.append(board[row + forward_dir][col - 1][self.FIELD_NAME])
        # capture right
        if col + 1 < 8 and board[row + forward_dir][col + 1][self.FIELD_OWNER] == opponent:
            available_moves.append(board[row + forward_dir][col + 1][self.FIELD_NAME])
        # two steps forward if starting position
        if ((row == 1 and forward_dir == 1) or (row == 6 and forward_dir == -1)) and board[row + forward_dir * 2][col][self.PIECE_NAME] == self.EMPTY and board[row + forward_dir][col][self.PIECE_NAME] == self.EMPTY:
            available_moves.append(board[row + forward_dir * 2][col][self.FIELD_NAME])

        return available_moves
    
    def _getKnightMoves(self, field, board):
        available_moves = []
        to_consider = []
        row, col = self.getRowColByField(field)
        if self.fieldHeldByWhite(field):
            opponent = self.BLACK
        else:
            opponent = self.WHITE
        #all posible moves list
        if row + 2 < 8 and col - 1 >= 0:
            up_left = board[row + 2][col - 1]
            to_consider.append(up_left)
        if row + 2 < 8 and col + 1 < 8:
            up_right = board[row + 2][col + 1]
            to_consider.append(up_right)
        if row + 1 < 8 and col - 2 >= 0:
            left_up = board[row + 1][col - 2]
            to_consider.append(left_up)
        if row - 1 >= 0 and col - 2 >= 0:
            left_down = board[row - 1][col - 2]
            to_consider.append(left_down)
        if row - 2 >= 0 and col - 1 >= 0:
            down_left = board[row - 2][col - 1]
            to_consider.append(down_left)
        if row - 2 >= 0 and col + 1 < 8:
            down_right = board[row - 2][col + 1]
            to_consider.append(down_right)
        if row + 1 < 8 and col + 2 < 8:
            right_up = board[row + 1][col + 2]
            to_consider.append(right_up)
        if row - 1 >= 0 and col + 2 < 8:
            right_down = board[row - 1][col + 2]
            to_consider.append(right_down)

        for field in to_consider:
            if field is not None and (not field[self.IS_TAKEN] or field[self.FIELD_OWNER] == opponent):
                available_moves.append(field[self.FIELD_NAME])

        return available_moves

    def _getRookMoves(self, field, board):
        available_moves = []
        to_consider = []
        row, col = self.getRowColByField(field)
        if self.fieldHeldByWhite(field):
            opponent = self.BLACK
        else:
            opponent = self.WHITE
        i = row + 1
        while i < 8:
            if board[i][col][self.FIELD_OWNER] == self.NEUTRAL:
                available_moves.append(board[i][col][self.FIELD_NAME])
            elif board[i][col][self.FIELD_OWNER] == opponent:
                available_moves.append(board[i][col][self.FIELD_NAME])
                break
            else:
                break
            i+=1
        i = row - 1
        while i >= 0:
            if board[i][col][self.FIELD_OWNER] == self.NEUTRAL:
                available_moves.append(board[i][col][self.FIELD_NAME])
            elif board[i][col][self.FIELD_OWNER] == opponent:
                available_moves.append(board[i][col][self.FIELD_NAME])
                break
            else:
                break
            i-=1
        j = col - 1
        while j >= 0:
            if board[row][j][self.FIELD_OWNER] == self.NEUTRAL:
                available_moves.append(board[row][j][self.FIELD_NAME])
            elif board[row][j][self.FIELD_OWNER] == opponent:
                available_moves.append(board[row][j][self.FIELD_NAME])
                break
            else:
                break
            j-=1
        j = col + 1
        while j < 8:
            if board[row][j][self.FIELD_OWNER] == self.NEUTRAL:
                available_moves.append(board[row][j][self.FIELD_NAME])
            elif board[row][j][self.FIELD_OWNER] == opponent:
                available_moves.append(board[row][j][self.FIELD_NAME])
                break
            else:
                break
            j+=1

        return available_moves

    def _getBishopMoves(self, field, board):
        available_moves = []
        to_consider = []
        row, col = self.getRowColByField(field)
        if self.fieldHeldByWhite(field):
            opponent = self.BLACK
        else:
            opponent = self.WHITE
        i = row + 1
        j = col + 1
        while i < 8 and j < 8:
            if board[i][j][self.FIELD_OWNER] == self.NEUTRAL:
                available_moves.append(board[i][j][self.FIELD_NAME])
            elif board[i][j][self.FIELD_OWNER] == opponent:
                available_moves.append(board[i][j][self.FIELD_NAME])
                break
            else:
                break
            i+=1
            j+=1
        i = row - 1
        j = col - 1
        while i >= 0 and j >=0:
            if board[i][j][self.FIELD_OWNER] == self.NEUTRAL:
                available_moves.append(board[i][j][self.FIELD_NAME])
            elif board[i][j][self.FIELD_OWNER] == opponent:
                available_moves.append(board[i][j][self.FIELD_NAME])
                break
            else:
                break
            i-=1
            j-=1
        i = row + 1
        j = col - 1
        while i < 8 and j >= 0:
            if board[i][j][self.FIELD_OWNER] == self.NEUTRAL:
                available_moves.append(board[i][j][self.FIELD_NAME])
            elif board[i][j][self.FIELD_OWNER] == opponent:
                available_moves.append(board[i][j][self.FIELD_NAME])
                break
            else:
                break
            i+=1
            j-=1
        i = row - 1
        j = col + 1
        while i >= 0 and j < 8:
            if board[i][j][self.FIELD_OWNER] == self.NEUTRAL:
                available_moves.append(board[i][j][self.FIELD_NAME])
            elif board[i][j][self.FIELD_OWNER] == opponent:
                available_moves.append(board[i][j][self.FIELD_NAME])
                break
            else:
                break
            i-=1
            j+=1

        return available_moves

    def _getQueenMoves(self, field, board):
        a = self._getRookMoves(field, board)
        b = self._getBishopMoves(field, board)

        return a + b

    def _getKingMoves(self, field, board):
        available_moves = []
        row, col = self.getRowColByField(field)
        if self.fieldHeldByWhite(field):
            opponent = self.BLACK
        else:
            opponent = self.WHITE
        #parallel
        if (row + 1 < 8) and (board[row + 1][col][self.FIELD_OWNER] == self.NEUTRAL or board[row + 1][col][self.FIELD_OWNER] == opponent):
            available_moves.append(board[row + 1][col][self.FIELD_NAME])
        if (row - 1 >= 0) and (board[row - 1][col][self.FIELD_OWNER] == self.NEUTRAL or board[row - 1][col][self.FIELD_OWNER] == opponent):
            available_moves.append(board[row - 1][col][self.FIELD_NAME])
        if (col + 1 < 8) and (board[row][col + 1][self.FIELD_OWNER] == self.NEUTRAL or board[row][col + 1][self.FIELD_OWNER] == opponent):
            available_moves.append(board[row][col + 1][self.FIELD_NAME])
        if (col - 1 >= 0) and (board[row][col - 1][self.FIELD_OWNER] == self.NEUTRAL or board[row][col - 1][self.FIELD_OWNER] == opponent):
            available_moves.append(board[row][col - 1][self.FIELD_NAME])
        #oblique 
        if (row + 1 < 8 and col + 1 < 8) and (board[row + 1][col + 1][self.FIELD_OWNER] == self.NEUTRAL or board[row + 1][col + 1][self.FIELD_OWNER] == opponent):
            available_moves.append(board[row + 1][col + 1][self.FIELD_NAME])
        if (row - 1 >= 0 and col - 1 >= 0) and (board[row - 1][col - 1][self.FIELD_OWNER] == self.NEUTRAL or board[row - 1][col - 1][self.FIELD_OWNER] == opponent):
            available_moves.append(board[row - 1][col - 1][self.FIELD_NAME])
        if (row + 1 < 8 and col - 1 >= 0) and (board[row + 1][col - 1][self.FIELD_OWNER] == self.NEUTRAL or board[row + 1][col - 1][self.FIELD_OWNER] == opponent):
            available_moves.append(board[row + 1][col - 1][self.FIELD_NAME])
        if (row - 1 >= 0 and col + 1 < 8) and (board[row - 1][col + 1][self.FIELD_OWNER] == self.NEUTRAL or board[row - 1][col + 1][self.FIELD_OWNER] == opponent):
            available_moves.append(board[row - 1][col + 1][self.FIELD_NAME])

        return available_moves