import cv2
import numpy as np

img = cv2.imread('storm2.jpg', 0)  # zdjecie czarn obiale
ret, thresh = cv2.threshold(img, 127, 255, 0)  # progowanie
contours, hierarchy = cv2.findContours(
    thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)  # szukanie kontur ??? co to 1 i2?

img_countours = np.zeros(img.shape)
for i in range(0, len(contours)):
    cnt = contours[i]
    # macierz momentów wszystkich obliczonych wartosci momentu
    M = cv2.moments(cnt)
    if M['m00'] != 0:

        cv2.drawContours(img_countours, [cnt], -1, (25, 255, 25), 3)
        cx = int(M['m10']/M['m00'])  # sam środek obiektu, ktory nas interesuje
        cy = int(M['m01']/M['m00'])

        area = cv2.contourArea(cnt)  # obszar konturu
        # perimeter = cv2.arcLength(cnt, True) # długość łuku, jezeli mamy zamkniety obiekt, ustawiamy true, jezeli mamy krzywa, ustawiamy False
        # print(area)
        # print(cv2.isContourConvex(cnt)) # czy są krzywe wypukłe? jezeli by byly mozna uzyc cv2.convexHull()
        # hull = cv2.convexHull(cnt)

        if area > 80:

            # rysuje zwykly prostokat, jezeli obiekt nie jest pochylony.
            x, y, w, h = cv2.boundingRect(cnt)
            img_countours = cv2.rectangle(
                img_countours, (x, y), (x+w, y+h), (75, 255, 25), 2)

            # mozemy sprawdzic jego aspect ratio
            # aspect_ratio = float(w)/h

            # rect_area = w*h # obszar konturu do ograniczającego obszaru prostokąta
            # extent = float(area)/rect_area

            # hull_area = cv2.contourArea(hull) # powierzchnia konturu do wypukłej powierzchni
            # solidity = float(area)/hull_area

            # equi_diameter = np.sqrt(4*area/np.pi) equivalent diameter - średnica okręgu, którego powierzchnia jest taka sama jak powierzchnia konturu

            # (x,y),(MA,ma),angle = cv2.fitEllipse(cnt) orientacja to kąt, pod jakim jest skierowany obiekt

            # mask = np.zeros(imgray.shape,np.uint8) # są to wszystkie punkty, które opisują nasz obiekt.
            # cv2.drawContours(mask,[cnt],0,255,-1)
            # pixelpoints = np.transpose(np.nonzero(mask))
            # pixelpoints = cv2.findNonZero(mask)

            # min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(imgray,mask = mask) min,max wartość i ich lokacja, musimy mieć obraz maski

            # mean_val = cv2.mean(im,mask = mask) # średnia arytmetyczna kolorów.

            # rotujący prostokąt
            rect = cv2.minAreaRect(cnt)  # minimalny powierzchnia prostokata
            box = cv2.boxPoints(rect)  # cztery punkty prostokata
            # zwraca, strukturę -> x,y, (width, height), kąt rotacji
            box = np.int0(box)
            img_countours = cv2.drawContours(
                img_countours, [box], 0, (125, 125, 255), 2)

            (x, y), radius = cv2.minEnclosingCircle(
                cnt)  # minimalne koło wokół obiektu
            center = (int(x), int(y))
            radius = int(radius)
            img_countours = cv2.circle(
                img_countours, center, radius, (2, 255, 0), 2)

            ellipse = cv2.fitEllipse(cnt)  # elipsa
            img_countours = cv2.ellipse(img_countours, ellipse, (2, 255, 0), 2)

            rows, cols = img.shape[:2]  # linia przez obiekt
            [vx, vy, x, y] = cv2.fitLine(cnt, cv2.DIST_L2, 0, 0.01, 0.01)
            lefty = int((-x*vy/vx) + y)
            righty = int(((cols-x)*vy/vx)+y)
            img_countours = cv2.line(
                img_countours, (cols-1, righty), (0, lefty), (2, 255, 0), 2)

            # extreme points
            leftmost = tuple(cnt[cnt[:, :, 0].argmin()][0])
            rightmost = tuple(cnt[cnt[:, :, 0].argmax()][0])
            topmost = tuple(cnt[cnt[:, :, 1].argmin()][0])
            bottommost = tuple(cnt[cnt[:, :, 1].argmax()][0])


cv2.imshow('img', img_countours)
cv2.waitKey(0)
cv2.destroyAllWindows()
