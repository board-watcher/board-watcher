import cv2 as cv
import sys
import numpy as np

def trackbarCallback(x):
    global slider_value
    if x % 2 == 0:
        slider_value = x + 1
    else:
        slider_value = x

slider_value = 5
cv.namedWindow('Kernel', cv.WINDOW_NORMAL)
cv.createTrackbar('Size', 'Kernel', 5, 15, trackbarCallback)
cam = cv.VideoCapture(0)
# width = cam.get(cv.CAP_PROP_FRAME_WIDTH)
# height = cam.get(cv.CAP_PROP_FRAME_HEIGHT)
mask = np.zeros((500, 500),np.uint8)

while(True):
    _, frame = cam.read()
    gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
    blur = cv.GaussianBlur(gray,(slider_value,slider_value),0)
    gaussian = cv.adaptiveThreshold(blur,255,cv.ADAPTIVE_THRESH_GAUSSIAN_C, cv.THRESH_BINARY,11,2)
    contour,hier = cv.findContours(gaussian,cv.RETR_TREE,cv.CHAIN_APPROX_SIMPLE)
    cv.imshow("Threshold - Gaussian method", gaussian)
        
    max_area = 0
    best_cnt = None
    for cnt in contour:
        area = cv.contourArea(cnt)
        if area > 1000:
            if area > max_area:
                max_area = area
                best_cnt = cnt

    cv.drawContours(mask,[best_cnt],0,255,-1)
    cv.drawContours(mask,[best_cnt],0,0,2)
    mask = cv.inRange(gaussian, 100, 200)
    result = cv.bitwise_and(frame, frame, mask=mask)

    cv.imshow("Contour", result)

    k = cv.waitKey(1) & 0xFF
    if k == 27: # Esc key
        break

    
cv.destroyAllWindows()