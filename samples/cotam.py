import numpy as np
import cv2


def order_points(pts):
    rect = np.zeros((4, 2), dtype="float32")

    my_array = np.array(pts)
    s = my_array.sum(axis=1)
    rect[0] = my_array[np.argmin(s)]
    rect[2] = my_array[np.argmax(s)]
    # now, compute the difference between the points, the
    # top-right point will have the smallest difference,
    # whereas the bottom-left will have the largest difference
    diff = np.diff(pts, axis=1)
    rect[1] = my_array[np.argmin(diff)]
    rect[3] = my_array[np.argmax(diff)]
    # return the ordered coordinates
    return rect


# podlacz kamere, indeks zerowy bo jedna kamera itd.. jezeli chcemy jakis plik to wklepujemy go zamiast zera
cap = cv2.VideoCapture(0)

# defnicja kodeka i  stworzenie obiektu VideoWriter

if cap.isOpened() == False:
    print("nie otworzylem kamery")


while True:
    # Capture frame-by-frame
    ret, frame = cap.read()  # frame otrzyma nastęną klatkę w aparacie przez "cap"
    # ret uzyska wartość zwracaną z pobrania ramki kamery, prawda lub fałsz.

    #frame = cv2.GaussianBlur(frame, (5, 5), 0)
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    mask = np.zeros((gray.shape), np.uint8)
    kernel1 = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (11, 11))

    #close = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel1)

    thresh = cv2.adaptiveThreshold(gray, 255, 0, 1, 19, 2)

    contour, hier = cv2.findContours(
        thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    max_area = 0
    best_cnt = None
    for cnt in contour:
        area = cv2.contourArea(cnt)
        if area > 1000:
            if area > max_area:
                max_area = area
                best_cnt = cnt

    cv2.drawContours(mask, [best_cnt], 0, 255, -1)
    cv2.drawContours(mask, [best_cnt], 0, 0, 2)

    res = cv2.bitwise_and(frame, frame, mask=mask)

    max_sum = 0
    min_sum = 10000
    max_diff = 0
    min_diff = 10000
    for point in best_cnt:
        if max_sum < point[0][0] + point[0][1]:
            max_sum = point[0][0] + point[0][1]
            botright = tuple((point[0][0], point[0][1]))
        if min_sum > point[0][0] + point[0][1]:
            min_sum = point[0][0] + point[0][1]
            topleft = tuple((point[0][0], point[0][1]))
        if max_diff < abs(point[0][0] - point[0][1]):
            max_diff = point[0][0] - point[0][1]
            topright = tuple((point[0][0], point[0][1]))
        if min_diff > point[0][0] - point[0][1]:
            min_diff = point[0][0] - point[0][1]
            botleft = tuple((point[0][0], point[0][1]))

    cv2.circle(res, botright, 5, (255, 0, 255), 2, cv2.LINE_AA)
    cv2.circle(res, topright, 5, (255, 0, 255), 2, cv2.LINE_AA)
    cv2.circle(res, topleft, 5, (255, 0, 255), 2, cv2.LINE_AA)
    cv2.circle(res, botleft, 5, (255, 0, 255), 2, cv2.LINE_AA)

    cv2.imshow('zzz', res)

    #print(leftmost, rightmost, topmost, bottommost)

    pts3 = np.float32([[topleft[0], topleft[1]], [topright[0], topright[1]], [
        botright[0], botright[1]], [botleft[0], botleft[1]]])

    pts4 = np.float32([[0, 0], [640, 0], [640, 480], [0, 480]])

    M4 = cv2.getPerspectiveTransform(pts3, pts4)
    dst4 = cv2.warpPerspective(res, M4, (640, 480))

    cv2.imshow('dst4', dst4)

    if cv2.waitKey(1) & 0xFF == ord('q'):  # 64bit wersja, 0xFF, weź pierwszy bit
        break


# When everything done, release the capture
cap.release()
# Closes all the frames
cv2.destroyAllWindows()
