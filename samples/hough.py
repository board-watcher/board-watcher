import cv2 as cv
import sys
import numpy as np
from functools import partial

class Hough:
    cannyMin = 30
    cannyMax = 100
    houghThreshold = 30
    houghMinLineLength = 30
    houghMaxLineGap = 10
    apertureSize = 3
    gaussian = 5       
    cam = cv.VideoCapture(0)

    def initGui(self):
        cv.namedWindow('Manipulators', cv.WINDOW_NORMAL)
        cv.createTrackbar('Gaussian', 'Manipulators', 5, 15, self.adjGaussian)
        cv.createTrackbar('CannyMin', 'Manipulators', 30, 100, self.adjCannyMin)
        cv.createTrackbar('CannyMax', 'Manipulators', 100, 200, self.adjCannyMax)
        cv.createTrackbar('HoughThreshold', 'Manipulators', 30, 200, self.adjHoughThreshold)
        cv.createTrackbar('HoughMinLineLength', 'Manipulators', 30, 200, self.adjHoughMinLineLength)
        cv.createTrackbar('HoughMaxLineGap', 'Manipulators', 10, 100, self.adjHoughMaxLineGap)
        cv.createTrackbar('ApertureSize', 'Manipulators', 0, 3, self.adjApertureSize)

    def adjHoughThreshold(self, x):
        self.houghThreshold = x
    def adjHoughMinLineLength(self, x):
        self.houghMinLineLength = x
    def adjHoughMaxLineGap(self, x):
        self.houghMaxLineGap = x
    def adjGaussian(self, x):
        self.gaussian = x + (x % 2) + 1
    def adjCannyMin(self, x):
        self.cannyMin = x
    def adjCannyMax(self, x):
        self.cannyMax = x
    def adjApertureSize(self, x):
        print(x)
        self.apertureSize = x + (x % 2) + 3

    def driver(self):
        while(True):
            _, origin = self.cam.read()
            gray = cv.cvtColor(origin, cv.COLOR_BGR2GRAY)
            blur = cv.GaussianBlur(gray,(self.gaussian,self.gaussian),0)
            img_gaussian = cv.adaptiveThreshold(blur,255,cv.ADAPTIVE_THRESH_GAUSSIAN_C, cv.THRESH_BINARY,11,2)
            cv.imshow("Threshold - Gaussian method", img_gaussian)
            img_edges = cv.Canny(img_gaussian, self.cannyMin, self.cannyMax, apertureSize=self.apertureSize)
            cv.imshow("Canny edges", img_edges)
            linesP = cv.HoughLinesP(img_edges, 1, np.pi / 180, self.houghThreshold, None, self.houghMinLineLength, self.houghMaxLineGap)

            if linesP is not None:
                for i in range(0, len(linesP)):
                    l = linesP[i][0]
                    cv.line(origin, (l[0], l[1]), (l[2], l[3]), (255,30,55), 3, cv.LINE_AA)
            
            cv.imshow("Edges", origin)

            k = cv.waitKey(1) & 0xFF
            if k == 27: # Esc key
                break



cv.destroyAllWindows()

hough = Hough()
hough.initGui()
hough.driver()
del hough