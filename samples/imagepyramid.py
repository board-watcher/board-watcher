import sys
import cv2

src = cv2.imread('ooo.png', 0)
while 1:
    rows, cols = src.shape
    cv2.imshow('Pyramids Demo', src)
    k = cv2.waitKey(0)
    if k == 27:
        break

    elif chr(k) == 'i':
        src = cv2.pyrUp(src, dstsize=(2 * cols, 2 * rows))
        print('** Zoom In: Image x 2')

    elif chr(k) == 'o':
        src = cv2.pyrDown(src, dstsize=(cols // 2, rows // 2))
        print('** Zoom Out: Image / 2')

cv2.destroyAllWindows()
