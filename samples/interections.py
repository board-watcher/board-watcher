import cv2 as cv
import sys
import numpy as np
import time
import math
from functools import partial
from collections import defaultdict
from timeit import default_timer as timer
import string
import random

class Intersections:
    cam = cv.VideoCapture(0)
    # cam.set(cv.CAP_PROP_FPS, 3)
    res_x, res_y, bits = cam.read()[1].shape

    threshold = 200
    rho = 1
    line_vis = True
    sampling = True
    current_line = 0
    alpha = 100
    beta = 1
    intersection_vis = False
    line_filter = False
    point_filter = False
    colors = None
    theta = 2

    def segment_by_angle_kmeans(self, lines, k=2, **kwargs):

        # Define criteria = (type, max_iter, epsilon)
        default_criteria_type = cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER
        criteria = kwargs.get('criteria', (default_criteria_type, 10, 1.0))
        flags = kwargs.get('flags', cv.KMEANS_RANDOM_CENTERS)
        attempts = kwargs.get('attempts', 10)

        # returns angles in [0, pi] in radians
        angles = np.array([line[0][1] for line in lines])
        # multiply the angles by two and find coordinates of that angle
        pts = np.array([[np.cos(2*angle), np.sin(2*angle)]
                        for angle in angles], dtype=np.float32)

        # run kmeans on the coords
        labels, centers = cv.kmeans(
            pts, k, None, criteria, attempts, flags)[1:]
        labels = labels.reshape(-1)  # transpose to row vec

        # segment lines based on their kmeans label
        segmented = defaultdict(list)  # slownik
        for i, line in zip(range(len(lines)), lines):
            segmented[labels[i]].append(line)  # label 0/1 i linia
        segmented = list(segmented.values())  # konwertowanie na listę
        return segmented


    def intersection(self, line1, line2):

        rho1, theta1 = line1[0]
        rho2, theta2 = line2[0]
        C = np.array([
            [np.cos(theta1), np.sin(theta1)],
            [np.cos(theta2), np.sin(theta2)]
        ])
        r = np.array([[rho1], [rho2]])
        wyznacznik = (C[0][0] * C[1][1]) - (C[0][1] * C[1][0])

        if wyznacznik != 0:
            x1, y1 = np.linalg.solve(C, r)
            x1, y1 = int(np.round(x1)), int(np.round(y1))
            return [x1, y1]


    def segmented_intersections(self, lines2):

        intersections2 = []
        for i, group in enumerate(lines2[:-1]):  # grupa z kemans pierwsza
            for next_group in lines2[i+1:]:  # grupa z kmeans druga
                for line1 in group:
                    for line2 in next_group:
                        if intersection(line1, line2) is not None:
                            intersections2.append(self.intersection(line1, line2))

        return intersections2


    # list_of_intersections has to be sorted
    def find_two_outer_corners(self, list_of_intersections, img):
        length = len(list_of_intersections)
        minvalue = list_of_intersections[0]
        maxvalue = list_of_intersections[length-1]
        cv.circle(img, (minvalue[0], minvalue[1]),
                   radius=6, color=(0, 0, 255), thickness=-1)  # min,min
        cv.circle(img, (maxvalue[0], maxvalue[1]),
                   radius=6, color=(0, 0, 255), thickness=-1)  # max,max

        return minvalue, maxvalue


    def findPerspectiveTransform(self, sortedX2, sortedY2, img):
        drawed_img = img.copy()
        min2, min3 = self.find_two_outer_corners(sortedY2, img)
        min22, min33 = self.find_two_outer_corners(sortedX2, img)

        pts3 = np.float32([[min2[0], min2[1]], [min33[0], min33[1]], [
            min22[0], min22[1]], [min3[0], min3[1]]])
        pts4 = np.float32([[50, 50], [750, 50], [50, 750], [750, 750]])
        M4 = cv.getPerspectiveTransform(pts3, pts4)
        dst4 = cv.warpPerspective(drawed_img, M4, (800, 800))
        return dst4

    def tranpose(self, img): 
        gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)

        x = cv.getTrackbarPos('X', 'namedTrackbar')
        y = cv.getTrackbarPos('Y', 'namedTrackbar')

        edges = cv.Canny(gray, x, y, apertureSize=3)
        lines = cv.HoughLines(edges, 1, np.pi/180, 150, None, 0, 0)
        if lines is not None:  # jezeli nie ma pustych linii to
            segmented = self.segment_by_angle_kmeans(lines)
            intersections = self.segmented_intersections(segmented)
            if len(intersections) >= 2 and intersections is not None:
                sortedY = sorted(intersections, key=lambda x: x[1])
                sortedX = sorted(intersections, key=lambda x: x[0])
                dst5 = self.findPerspectiveTransform(sortedX, sortedY, img)
                return dst5

    def nothing(self):
        pass;

    def initGui(self):
        cv.namedWindow('namedTrackbar', cv.WINDOW_FREERATIO)
        cv.createTrackbar('Threshold', 'namedTrackbar', 200, 300, self.adjThreshold)
        cv.createTrackbar('Alpha', 'namedTrackbar', 100, 300, self.adjAlpha)
        cv.createTrackbar('Beta', 'namedTrackbar', 1, 200, self.adjBeta)
        cv.createTrackbar('Lines', 'namedTrackbar', 1, 1, self.showLines)
        cv.createTrackbar('Intersections', 'namedTrackbar', 0, 1, self.showIntersections)
        cv.createTrackbar('Sampling', 'namedTrackbar', 0, 1, self.stopSampling)
        cv.createTrackbar('LineIndex', 'namedTrackbar', 0, 40, self.adjCurrentLine)
        cv.createTrackbar('LineFilter', 'namedTrackbar', 0, 1, self.toggleFilterLines)
        cv.createTrackbar('PointFilter', 'namedTrackbar', 0, 1, self.toggleFilterPoints)
        cv.createTrackbar('Theta', 'namedTrackbar', 0, 100, self.toggleTheta)
        cv.createTrackbar('Exposure', 'namedTrackbar', int(-self.cam.get(cv.CAP_PROP_EXPOSURE)), 10, self.setExposure)
        cv.createTrackbar('X', 'namedTrackbar', 100, 800, self.nothing)
        cv.createTrackbar('Y', 'namedTrackbar', 100, 800, self.nothing)
        
    def setExposure(self, x):
        self.cam.set(cv.CAP_PROP_EXPOSURE, float(-x))

    def adjThreshold(self, x):
        self.threshold = x
        # self.x = x + (x % 2) + 1

    def toggleFilterLines(self, x):
        self.line_filter = not self.line_filter

    def toggleTheta(self, x):
        self.theta = x / 10

    def toggleFilterPoints(self, x):
        self.point_filter = not self.point_filter
    
    def showLines(self, x):
        self.line_vis = not self.line_vis

    def showIntersections(self, x):
        self.intersection_vis = not self.intersection_vis

    def stopSampling(self, x):
        self.sampling = not self.sampling

    def adjCurrentLine(self, x):
        self.current_line = x

    def adjAlpha(self, x):
        self.alpha = x

    def adjBeta(self, x):
        self.beta = x

    def getKeyCode(self):
        return cv.waitKey(10) & 0xFF
                

    def segment_by_angle_kmeans(self, lines, k=2, **kwargs):
        """Groups lines based on angle with k-means.

        Uses k-means on the coordinates of the angle on the unit circle 
        to segment `k` angles inside `lines`.
        """

        # Define criteria = (type, max_iter, epsilon)
        default_criteria_type = cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER
        criteria = kwargs.get('criteria', (default_criteria_type, 10, 1.0))
        flags = kwargs.get('flags', cv.KMEANS_RANDOM_CENTERS)
        attempts = kwargs.get('attempts', 10)

        # returns angles in [0, pi] in radians
        angles = np.array([line[0][1] for line in lines])
        # multiply the angles by two and find coordinates of that angle
        pts = np.array([[np.cos(2*angle), np.sin(2*angle)]
                        for angle in angles], dtype=np.float32)

        # run kmeans on the coords
        labels, centers = cv.kmeans(pts, k, None, criteria, attempts, flags)[1:]
        labels = labels.reshape(-1)  # transpose to row vec

        # segment lines based on their kmeans label
        segmented = defaultdict(list)
        for i, line in zip(range(len(lines)), lines):
            segmented[labels[i]].append(line)
        segmented = list(segmented.values())
        return segmented

    def intersection(self, line1, line2):
        """Finds the intersection of two lines given in Hesse normal form.

        Returns closest integer pixel locations.
        See https://stackoverflow.com/a/383527/5087436
        """
        rho1, theta1 = line1[0]
        rho2, theta2 = line2[0]
        A = np.array([
            [np.cos(theta1), np.sin(theta1)],
            [np.cos(theta2), np.sin(theta2)]
        ])
        b = np.array([[rho1], [rho2]])
        try:
            x0, y0 = np.linalg.solve(A, b)
        except Exception: 
            solution, _, _, _ = np.linalg.lstsq(A, b, rcond=None)
            x0, y0 = solution
        
        x0, y0 = int(np.round(x0)), int(np.round(y0))
        if x0 < -1500 or x0 > 1500 or y0 < -1500 or y0 > 1500:
            return None
        return x0, y0


    def segmented_intersections(self, lines):
        """Finds the intersections between groups of lines."""

        intersections = []
        for i, group in enumerate(lines[:-1]):
            for next_group in lines[i+1:]:
                for line1 in group:
                    for line2 in next_group:
                        if(self.intersection(line1, line2) != None):
                            intersections.append(self.intersection(line1, line2)) 

        return intersections

    def sampleCameraFrame(self):
        _, img = self.cam.read()

        return img

    def getLines(self, img):
        gray = cv.cvtColor(img,cv.COLOR_BGR2GRAY)
        edges = cv.Canny(gray,50,150,apertureSize = 3)
        lines = cv.HoughLines(edges,self.rho,np.pi/180, self.threshold)

        return lines

    def getColor(self, i, current_line):
        if(current_line == i):
            color = (0, 255, 0)
        else:
            color = (0, 0, 255)
            # color = self.colors[i]
        
        return color

    def drawLines(self, lines, img):
        if(self.line_vis):
            for i, line in enumerate(lines):
                rho,theta = line[0]
                a = np.cos(theta)
                b = np.sin(theta)
                x0 = a*rho
                y0 = b*rho
                x1 = int(x0 + 1000*(-b))
                y1 = int(y0 + 1000*(a))
                x2 = int(x0 - 1000*(-b))
                y2 = int(y0 - 1000*(a))
                cv.line(img,(x1,y1),(x2,y2), self.getColor(i, self.current_line),2)

    def drawPoints(self, points, img, r, g, b):
        if(self.intersection_vis):
            for i, intersection in enumerate(points):
                cv.circle(img, (intersection[0], intersection[1]), 5, (b, g, r), -1)

    def drawIndex(self, points, img, r, g, b):
            if(self.intersection_vis):
                for i, intersection in enumerate(points):
                    cv.putText(img, str(i),(intersection[0], intersection[1]), cv.FONT_HERSHEY_SIMPLEX, 0.5,(b, g, r),2,cv.LINE_AA)



    def drawImg(self, window_name, img):
        cv.imshow(window_name, img)

    def dynamicBrightness(self, img):
        processed_img = np.zeros(img.shape, img.dtype)
        alpha = self.alpha / 100
        beta = self.beta
        processed_img = cv.convertScaleAbs(img, alpha=alpha, beta=beta)

        return processed_img

    def match_template(self, image, template):
        return cv.matchTemplate(image, template, cv.TM_CCOEFF_NORMED) 

    def filterLines(self, lines):
        arr = []
        aux = []
        for i, x in enumerate(lines):
            arr.append(x)
            
        for i, x in enumerate(arr):
            del_count = 0
            for j, y in enumerate(arr[i+1:]):
                if np.logical_and(abs(x[0][0] - y[0][0]) < 5, abs(x[0][1] - y[0][1]) < self.theta):
                    try:
                        del arr[i + 1 + j - del_count]
                        del_count = del_count + 1
                    except Exception:
                        print("Attempt to delete cos co not exists")
        return np.array(arr)

    def filterIntersections(self, intersections):
        aux = []
        res = []
        org = intersections
            
        for i, point in enumerate(intersections):
            del_count = 0
            aux.append(point)
            for j, iterated_point in enumerate(intersections[i+1:]):
                if(abs(point[0] - iterated_point[0]) < 20 and abs(point[1] - iterated_point[1]) < 20):
                    aux.append(iterated_point)
                    #intersections.remove(iterated_point)
                    del intersections[i + 1 + j - del_count]
                    del_count = del_count + 1
                    
            x, y = np.mean(aux, axis=0)
            res.append((int(x), int(y)))
            aux = []

        return np.array(res)

        

    # def draw(self, org, img, blank, blank_sorted):
    #     self.drawImg("Org", org)
    #     self.drawImg("Img", img)
    #     self.drawImg("Blank", blank)
    #     self.drawImg("BlankSorted", blank_sorted)
    #     blank = np.zeros((700,700,3), np.uint8)
    #     blank_sorted = np.zeros((700,700,3), np.uint8)

    def driver(self):
        intersections = None
        points = None
        blank = np.zeros((self.res_x,self.res_y,self.bits), np.uint8)
        # red_mask = np.zeros()
        while(True):
            if(self.sampling):
                org = self.sampleCameraFrame()
                transoned = self.tranpose(org)
                if transoned is not None:
                    img = self.dynamicBrightness(transoned)
                else:
                    img = self.dynamicBrightness(org)
                lines = self.getLines(img)
                # self.colors = [0, 0, 255] * len(lines)
                
                if(lines is not None and len(lines) < 200):
                    if(self.line_filter):
                        lines = self.filterLines(lines)
                        
                    segmented = self.segment_by_angle_kmeans(lines)
                    intersections = self.segmented_intersections(segmented)
                    if(self.point_filter):
                        intersections = self.filterIntersections(intersections)
                    intersections = sorted(intersections , key=lambda k: [k[1], k[0]])
                    self.drawLines(lines, img)
                    self.drawPoints(intersections, img, 0, 0, 255)
                    if(len(intersections) == 121):
                        cv.setTrackbarPos('Sampling', 'namedTrackbar', 1)
            else:
                self.drawLines(lines, img)
            if(intersections is not None):
                # self.drawPoints(points, img, 255, 0, 0)
                self.drawIndex(intersections, blank, 255, 255, 255)

            # pytesseract.pytesseract.tesseract_cmd = 'C:\Program Files\Tesseract-OCR\\tesseract.exe'
            # h, w, c = img.shape
            # boxes = pytesseract.image_to_boxes(img) 
            # for b in boxes.splitlines():
            #     b = b.split(' ')
            #     img = cv.rectangle(org, (int(b[1]), h - int(b[2])), (int(b[3]), h - int(b[4])), (0, 255, 0), 2)



            self.drawImg("Org", org)
            self.drawImg("Img", img)
            self.drawImg("Blank", blank)
            blank = np.zeros((self.res_x * 2,self.res_y * 2,self.bits), np.uint8)
            # self.draw(org, img, blank, blank_sorted)
            key = self.getKeyCode()

            if key == 27: # Esc key
                    break

intersections = Intersections()
intersections.initGui()
intersections.driver()
del intersections
cv.destroyAllWindows()
