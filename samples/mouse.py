import cv2
import numpy as np
import math


def nothing(x):
    pass


lista = []
ix, iy = -1, -1
licz = 0


def draw_circle(event, x, y, flags, param):
    global ix, iy, mode, index, dl_pom, flaga, licz
    if event == cv2.EVENT_LBUTTONDOWN:
        if len(lista) == 0:
            lista.append([x, y])

        if len(lista) >= 1:
            for i in range(0, len(lista)):
                dl = math.sqrt(((lista[i][0]-x) ** 2) + ((lista[i][1]-y) ** 2))
                # jezeli dlugosci miedzy wszystkimi zaznaczonymi punktami są większe od 15 to zwiększ licznik o 1
                if dl > 15 and len(lista) < 4:
                    licz += 1
                elif dl < 25:
                    mode = True
                    index = i

            if licz == len(lista):
                lista.append([x, y])

            licz = 0

    if event == cv2.EVENT_LBUTTONUP:
        if mode == True:
            lista.remove(lista[index])
            lista.append([ix, iy])
            mode = False

    if event == cv2.EVENT_MOUSEMOVE:
        ix, iy = x, y

    if event == cv2.EVENT_RBUTTONDOWN:
        for i in range(0, len(lista)):
            dl = math.sqrt(((lista[i][0]-x) ** 2) + ((lista[i][1]-y) ** 2))
            if dl < 20:
                lista.remove(lista[i])
                break


def safeMode(f):
    max_sum = 0
    min_sum = 10000
    max_diff = 0
    min_diff = 10000
    for point in lista:
        if max_sum < point[0] + point[1]:
            max_sum = point[0] + point[1]
            botright = tuple((point[0], point[1]))
        if min_sum > point[0] + point[1]:
            min_sum = point[0] + point[1]
            topleft = tuple((point[0], point[1]))
        if max_diff < abs(point[0] - point[1]):
            max_diff = point[0] - point[1]
            topright = tuple((point[0], point[1]))
        if min_diff > point[0] - point[1]:
            min_diff = point[0] - point[1]
            botleft = tuple((point[0], point[1]))

    pts3 = np.float32([[botleft[0], botleft[1]], [topleft[0], topleft[1]], [topright[0], topright[1]], [
        botright[0], botright[1]]])

    pts4 = np.float32([[0, 0], [640, 0], [640, 480], [0, 480]])

    M4 = cv2.getPerspectiveTransform(pts3, pts4)
    dst4 = cv2.warpPerspective(f, M4, (640, 480))
    return dst4


cap = cv2.VideoCapture(0)

cv2.namedWindow('frame')
cv2.createTrackbar('Tryb awaryjny', 'frame', 0, 1, nothing)


if cap.isOpened() == False:
    print("nie otworzylem kamery")

while True:
    ret, frame = cap.read()

    position = cv2.getTrackbarPos('Tryb awaryjny', 'frame')
    if position == 1:
        cv2.setMouseCallback('frame', draw_circle)
        for i in range(0, len(lista)):
            cv2.circle(frame, (lista[i][0], lista[i][1]), 5, (0, 0, 255), 2)

        if len(lista) == 4:
            perspective = safeMode(frame)
            cv2.imshow('perspective', perspective)
        else:
            cv2.destroyWindow('perspective')

    cv2.imshow('frame', frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):  # 64bit wersja, 0xFF, weź pierwszy bit
        break

# When everything done, release the capture
cap.release()
# Closes all the frames
cv2.destroyAllWindows()
