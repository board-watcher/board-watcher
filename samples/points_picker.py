#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os.path
sys.path.append(str(os.path.abspath(os.path.dirname(__file__)) + '/../'))


import cv2 as cv
import numpy as np

from boardwatcher import transformations, lines as l, chess



def pick_points(points): 
    c = points - [400, 400]
    center_ind = np.argwhere(np.sqrt(np.sum(c*c, axis =1)) == np.min(np.sqrt(np.sum(c*c, axis=1))))
    center = points[center_ind][0][0]

    top_x = []
    top_y = []
    bottom_x = []
    bottom_y = []

    for point in points:
        if np.all(point == center):
            continue
        if point[0] > center[0] and (point[1] < center[1] + 15 and point[1] > center[1] - 15):
            top_x.append(point)
        if point[0] < center[0] and (point[1] < center[1] + 15 and point[1] > center[1] - 15):
            bottom_x.append(point)
        if point[1] > center[1] and (point[0] < center[0] + 15 and point[0] > center[0] - 15):
            top_y.append(point)
        if point[1] < center[1] and (point[0] < center[0] + 15 and point[0] > center[0] - 15):
            bottom_y.append(point)

    top_x = sorted(top_x, key= lambda k: k[0])[:4]
    bottom_x = sorted(bottom_x, key= lambda k: k[0], reverse= True)[:4]
    top_y = sorted(top_y, key= lambda k: k[1])[:4]
    bottom_y = sorted(bottom_y, key= lambda k: k[1], reverse= True)[:4]


    picked_points = []
    for x, _ in top_x + bottom_x + [center]:
        for _, y in top_y + bottom_y + [center]:
            picked_points.append([x, y])
    

    return picked_points;



rho = 1
threshold = 180
theta = 2

img = cv.imread('samples/images/ch1_6.jpg')
transposed = transformations.tranpose(img, 200, 200)

lines = l.getLines(transposed, rho, threshold)

if(lines is not None):
    lines = l.filterLines(lines, theta)
    segmented = l.segment_by_angle_kmeans(lines)
    intersections = l.segmented_intersections(segmented)
    intersections = l.filterIntersections(intersections)


    picked = pick_points(intersections)
    l.drawPoints(intersections, transposed, 0, 255, 0, True)
    l.drawPoints(picked, transposed, 255, 0, 0, True)


while True:
    cv.imshow('img', transposed)


    k = cv.waitKey()
    if k == 27:
        break;
