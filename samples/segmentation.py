import numpy as np
import cv2 as cv
from matplotlib import pyplot as plt

img = cv.imread('samples/images/black-on-white.png')
gray = cv.cvtColor(img,cv.COLOR_BGR2GRAY)
gray = cv.GaussianBlur(gray, (5,5), 0)
ret, thresh = cv.threshold(gray,0,255,cv.THRESH_BINARY_INV+cv.THRESH_OTSU)

cv.imshow('img', thresh)
print(cv.countNonZero(thresh) / thresh.size)
cv.waitKey(0)
