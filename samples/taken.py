import cv2 as cv
import numpy as np

def getSampleHSVColor(img):
    img = cv.GaussianBlur(img, (5, 5), 0)
    hsv = cv.cvtColor(img, cv.COLOR_BGR2HSV)
    data = np.reshape(hsv, (-1, 3))
    data = np.float32(data)

    criteria = (cv.TermCriteria_EPS + cv.TERM_CRITERIA_MAX_ITER, 10, 1.0)
    flags = cv.KMEANS_RANDOM_CENTERS
    _, _, centers = cv.kmeans(data, 1, None, criteria, 10, flags)
    return centers[0].astype(np.int32)

def isTaken(img, color):
    taken = False

    lower_threshold = np.array([color[0] - 5, 0, color[2] - 15])
    upper_threshold = np.array([color[0] + 5, 255, color[2] + 15])
    hsv = cv.cvtColor(img, cv.COLOR_BGR2HSV)

    mask = cv.inRange(hsv, lower_threshold, upper_threshold)

    ratio_black = 1 -cv.countNonZero(mask)/(img.size / 3)
    return ratio_black > 0.60



empty = cv.imread('samples/images/none-on-black.png')

taken = cv.imread('samples/images/white-on-black.png')
blac_on = cv.imread('samples/images/black-on-black.png')
not_on = cv.imread('samples/images/black-not-on-black.png')
not_on_w = cv.imread('samples/images/white-not-on-black.png')
shadow = cv.imread('samples/images/shadow-on-black.png')

sample = getSampleHSVColor(empty)
print(sample)

print('taken has ratio: ', isTaken(taken, sample))
print('black_on has ratio: ', isTaken(blac_on, sample))
print('not_on has ratio: ', isTaken(not_on, sample))
print('not_on_w has ratio: ', isTaken(not_on_w, sample))
print('shadow has ratio: ', isTaken(shadow, sample))
print('empty has ratio: ', isTaken(empty, sample))
