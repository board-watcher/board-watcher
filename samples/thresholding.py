import cv2 as cv
import sys
import numpy as np

def trackbarCallback(x):
    global slider_value
    if x % 2 == 0:
        slider_value = x + 1
    else:
        slider_value = x

slider_value = 5
cv.namedWindow('Kernel', cv.WINDOW_NORMAL)
cv.createTrackbar('Size', 'Kernel', 5, 15, trackbarCallback)

cap = cv.VideoCapture(0)

while(True):
    _, frame = cap.read()
    gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
    blur = cv.GaussianBlur(gray,(slider_value,slider_value),0)
    _,otsu = cv.threshold(blur,0, 255,cv.THRESH_BINARY+cv.THRESH_OTSU)
    gaussian = cv.adaptiveThreshold(blur,255,cv.ADAPTIVE_THRESH_GAUSSIAN_C, cv.THRESH_BINARY,11,2)
    cv.imshow("Threshold - Otsu method", otsu)
    cv.imshow("Threshold - Gaussian method", gaussian)

    k = cv.waitKey(1) & 0xFF
    if k == 27: # Esc key
        break

    
cv.destroyAllWindows()