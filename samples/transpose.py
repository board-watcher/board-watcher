from __future__ import division
import numpy as np
import cv2
from collections import defaultdict
from matplotlib import pyplot as plt


def nothing(x):
    pass


def segment_by_angle_kmeans(lines, k=2, **kwargs):

    # Define criteria = (type, max_iter, epsilon)
    default_criteria_type = cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER
    criteria = kwargs.get('criteria', (default_criteria_type, 10, 1.0))
    flags = kwargs.get('flags', cv2.KMEANS_RANDOM_CENTERS)
    attempts = kwargs.get('attempts', 10)

    # returns angles in [0, pi] in radians
    angles = np.array([line[0][1] for line in lines])
    # multiply the angles by two and find coordinates of that angle
    pts = np.array([[np.cos(2*angle), np.sin(2*angle)]
                    for angle in angles], dtype=np.float32)

    # run kmeans on the coords
    labels, centers = cv2.kmeans(
        pts, k, None, criteria, attempts, flags)[1:]
    labels = labels.reshape(-1)  # transpose to row vec

    # segment lines based on their kmeans label
    segmented = defaultdict(list)  # slownik
    for i, line in zip(range(len(lines)), lines):
        segmented[labels[i]].append(line)  # label 0/1 i linia
    segmented = list(segmented.values())  # konwertowanie na listę
    return segmented


def intersection(line1, line2):

    rho1, theta1 = line1[0]
    rho2, theta2 = line2[0]
    C = np.array([
        [np.cos(theta1), np.sin(theta1)],
        [np.cos(theta2), np.sin(theta2)]
    ])
    r = np.array([[rho1], [rho2]])
    wyznacznik = (C[0][0] * C[1][1]) - (C[0][1] * C[1][0])

    if wyznacznik != 0:
        x1, y1 = np.linalg.solve(C, r)
        x1, y1 = int(np.round(x1)), int(np.round(y1))
        return [x1, y1]


def segmented_intersections(lines2):

    intersections2 = []
    for i, group in enumerate(lines2[:-1]):  # grupa z kemans pierwsza
        for next_group in lines2[i+1:]:  # grupa z kmeans druga
            for line1 in group:
                for line2 in next_group:
                    if intersection(line1, line2) is not None:
                        intersections2.append(intersection(line1, line2))

    return intersections2


# list_of_intersections has to be sorted
def find_two_outer_corners(list_of_intersections):
    length = len(list_of_intersections)
    minvalue = list_of_intersections[0]
    maxvalue = list_of_intersections[length-1]
    cv2.circle(frame, (minvalue[0], minvalue[1]),
               radius=6, color=(0, 0, 255), thickness=-1)  # min,min
    cv2.circle(frame, (maxvalue[0], maxvalue[1]),
               radius=6, color=(0, 0, 255), thickness=-1)  # max,max

    return minvalue, maxvalue


def findPerspectiveTransform(sortedX2, sortedY2):

    min2, min3 = find_two_outer_corners(sortedY2)
    min22, min33 = find_two_outer_corners(sortedX2)

    pts3 = np.float32([[min2[0], min2[1]], [min33[0], min33[1]], [
        min22[0], min22[1]], [min3[0], min3[1]]])
    pts4 = np.float32([[0, 0], [800, 0], [0, 800], [800, 800]])
    M4 = cv2.getPerspectiveTransform(pts3, pts4)
    dst4 = cv2.warpPerspective(frame, M4, (800, 800))
    return dst4


cap = cv2.VideoCapture(0)
if cap.isOpened() == False:
    print("nie otworzylem kamery")

def setExposure(x):
    cap.set(cv2.CAP_PROP_EXPOSURE, float(-x))

    
cv2.namedWindow('frame')
cv2.createTrackbar('X', 'frame', 100, 800, nothing)
cv2.createTrackbar('Y', 'frame', 100, 800, nothing)
cv2.createTrackbar('exposure', 'frame', 1, 10, setExposure)


    
while True:
    # Capture frame-by-frame
    ret, frame = cap.read()  # frame otrzyma nastęną klatkę w aparacie przez "cap"

    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    x = cv2.getTrackbarPos('X', 'frame')
    y = cv2.getTrackbarPos('Y', 'frame')

    edges = cv2.Canny(gray, x, y, apertureSize=3)
    lines = cv2.HoughLines(edges, 1, np.pi/180, 150, None, 0, 0)
    if lines is not None:  # jezeli nie ma pustych linii to
        segmented = segment_by_angle_kmeans(lines)
        intersections = segmented_intersections(segmented)
        if len(intersections) >= 2 and intersections is not None:
            sortedY = sorted(intersections, key=lambda x: x[1])
            sortedX = sorted(intersections, key=lambda x: x[0])
            dst5 = findPerspectiveTransform(sortedX, sortedY)
            cv2.imshow('dst5', dst5)

    cv2.imshow('frame', frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):  # 64bit wersja, 0xFF, weź pierwszy bit
        break
# When everything done, release the capture
cap.release()
# Closes all the frames
cv2.destroyAllWindows()
